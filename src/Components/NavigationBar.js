import React from 'react'
import { Navbar, Nav, NavDropdown } from 'react-bootstrap'
import './Styles/Navigationbar.css'

//this is users navigation bar after logged in
class NavigationBar extends React.Component {
    render() {
        return (
            <div className="navbar" >
            <Navbar bg="dark" variant="dark">
                <div className="logo">
                <Navbar.Brand href="/user/dashboard" className="navbar-logo">AUTOWORKS</Navbar.Brand>
                </div>
                <div className="nav_items">
                <Navbar.Collapse>
                    <div className="nav_dropdown_items">
                    <Nav>
                        <Nav.Link href="/user/dashboard" style={{color:"white"}}>Dashboard</Nav.Link>
                        <NavDropdown title={
                            <span style={{color:"white"}}>Tasks</span>
                        } id="basic-nav-dropdown" >
                            <NavDropdown.Item href="/user/task/addtask">Add New Task</NavDropdown.Item>
                            <NavDropdown.Item href="/user/task/listtasks">View Tasks</NavDropdown.Item>
                            <NavDropdown.Item href="/user/task/viewinvoices">View Invoices</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title={
                            <span style={{color:"white"}}>My Data</span>
                        } id="basic-nav-dropdown" >
                            <NavDropdown.Item href="/user/mydata/customers">Customers</NavDropdown.Item>
                            <NavDropdown.Item href="/user/mydata/vehicles">Vehicles</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title={
                            <span style={{color:"white"}}>Inventory</span>
                        } id="basic-nav-dropdown" >
                            <NavDropdown.Item href="/user/inventory/additem">Add Items</NavDropdown.Item>
                            <NavDropdown.Item href="/user/inventory/items">View Items</NavDropdown.Item>
                            <NavDropdown.Item href="/user/inventory/categories">View Categories</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title={
                            <span style={{color:"white"}}>User</span>
                        } id="basic-nav-dropdown" >
                            <NavDropdown.Item href="/user/profile">Profile</NavDropdown.Item>
                            <NavDropdown.Item href="/signin">Log Out</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    </div>
                </Navbar.Collapse>
                </div>
            </Navbar>
            </div>
        );
    }
}

export default NavigationBar