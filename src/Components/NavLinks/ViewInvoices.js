import React, { Component } from 'react'
import axios from 'axios'
import Swal from 'sweetalert2'
import { Table, Button} from 'react-bootstrap'
import Background from '../../Images/background.jpg'
import { Spin } from "antd"
import "antd/dist/antd.css"
import 'bootstrap/dist/css/bootstrap.css';

class ViewInvoices extends Component {
//assigning initial states for the values
  constructor(props) {
    super(props);
    this.state = {
      taskinvoicedata: [],
      loading: false,
      userdata: JSON.parse(sessionStorage.getItem("usermail")),
    }
  }
//control the data when rendering this page
  componentDidMount() {
    var self = this
    //get all original completed task list for the user
    axios.get(`http://localhost:8401/task/getallinvoicesbyemail?email=${this.state.userdata}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ taskinvoicedata: response.data });
        } else {
          self.setState({ loading: true });
          Swal.fire("No Completed Tasks Available!", "", "error");
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ loading: true });
        Swal.fire("Error!", "Server Failed!", "error");
      });


  }



//render the customer list on the table
  render() {
    const details = this.state.taskinvoicedata.map((task) => (
      <tr style={{display:'table', width:'100%',tableLayout:'fixed'}}>
        <td>{task.regNo}</td>
        <td>{task.customerName}</td>
        <td>{task.completedDate}</td>
        <td>Rs. {task.task.totalCost}</td>
        <td align="center"><Button variant="info" href={`http://localhost:8401/task/uploads/${this.state.userdata}/${task.task.id}`}>Show</Button></td>
      </tr>
    ))

    return (
      <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
        <div style={{ textAlign: "center", marginTop: "1.5rem", fontFamily: "sans-serif" }}><h1 style={{color:'white'}}>Invoice Details</h1></div>
        <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>

          <div style={{ margin: "2rem" }}>
            <Table width="100%" variant="dark" bordered="true">
              <thead style={{display:'table', width:'100%',tableLayout:'fixed'}}>
                <tr style={{ backgroundColor: "#9d46fa" }}>
                  <th >Vehicle Reg No :</th>
                  <th >Customer Name:</th>
                  <th >Date:</th>
                  <th>Cost:</th>
                  <th >Invoice:</th>
                </tr>
              </thead>
              <tbody style={{ display: 'block', overflowY: 'scroll', height: '500px' }}>
                {details}
              </tbody>

            </Table>
          </div>
        </Spin>
      </div>
    )
  }

}

export default ViewInvoices