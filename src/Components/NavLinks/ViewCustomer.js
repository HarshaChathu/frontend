import React, { Component } from 'react'
import axios from 'axios'
import Swal from 'sweetalert2'
import { Table, Button} from 'react-bootstrap'
import Background from '../../Images/background.jpg'
import { Spin } from "antd"
import "antd/dist/antd.css"
import 'bootstrap/dist/css/bootstrap.css';

class ViewCustomer extends Component {
//assigning initial states for the values
  constructor(props) {
    super(props);
    this.state = {
      customerdata: [],
      loading: false,
      userdata: JSON.parse(sessionStorage.getItem("usermail")),
    }
  }
//control the data when rendering this page
  componentDidMount() {
    var self = this
    //get all customer list for the user
    axios.get(`http://localhost:8401/task/getallcustomersbyemail?email=${this.state.userdata}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ customerdata: response.data });
        } else {
          self.setState({ loading: true });
          Swal.fire("No Customers for you!", "", "error");
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ loading: true });
        Swal.fire("Error!", "Server Failed!", "error");
      });


  }



//render the customer list on the table
  render() {
    console.log(this.state.customerdata);
    const details = this.state.customerdata.map((customer, i) => (
      <tr key={i} style={{display:'table', width:'100%',tableLayout:'fixed'}}>
        {
          customer.map((num, j) =>
            <td key={j}>{num}</td>
          )
        }
        <td align="center">{/*updateing the customer data*/ }
          <Button onClick={() => {
            (async () => {
              const { value: formValues } = await Swal.fire({
                title: `Customer : ${customer[1]}`,
                html: `<table className="viewTable" cellpadding="5" cellspacing="5">
                                    <tr>
                                      <td align="left">NIC :</td>
                                      <td align="left" className="leftPad"><input disabled value="${customer[0]}" id="swal-input1" class="swal2-input"></td>
                                    </tr>
                                    <tr>
                                      <td align="left">Name :</td>
                                      <td align="left" className="leftPad"><input value="${customer[1]}" id="swal-input2" class="swal2-input"></td>
                                    </tr>
                                    <tr>
                                      <td align="left">Address :</td>
                                      <td align="left" className="leftPad"><input value="${customer[2]}" id="swal-input3" class="swal2-input"></td>
                                    </tr>
                                    <tr>
                                      <td align="left">Phone No :</td>
                                      <td align="left" className="leftPad"><input value="${customer[3]}" id="swal-input4" class="swal2-input"></td>
                                    </tr>
                                </table>`,
                focusConfirm: false,
                confirmButtonText: `Save`,
                showCancelButton: true,
                preConfirm: () => {
                  return [
                    document.getElementById("swal-input1").value,
                    document.getElementById("swal-input2").value,
                    document.getElementById("swal-input3").value,
                    document.getElementById("swal-input4").value
                  ];
                }
              });
            
              if (formValues) {
                //send updated customer data to server and update data
                axios.post(`http://localhost:8401/customer/update?name=${formValues[1]}&address=${formValues[2]}&phoneNo=${formValues[3]}&nic=${formValues[0]}`)
                                    .then(res => {
                                        if (res.status === 201 || res.status === 200) {
                                            Swal.fire(
                                                'Updated!',
                                                'Customer data Updated',
                                                'success'
                                            )
                                            this.componentDidMount();//re render the page
                                        }
                                    })
                                    .catch(err => {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Server error occured',
                                            html: '<p>Try again in few minutes</p>'
                                        })
                                    });
              }
            })();
          }}>Edit</Button>
        </td>
      </tr>
    ))

    return (
      <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
        <div style={{ textAlign: "center", marginTop: "1.5rem", fontFamily: "sans-serif" }}><h1 style={{color:'white'}}>Customers</h1></div>
        <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>

          <div style={{ margin: "2rem" }}>
            <Table width="100%" variant="dark" bordered="true">
              <thead style={{display:'table', width:'100%',tableLayout:'fixed'}}>
                <tr style={{ backgroundColor: "#9d46fa" }}>
                  <th >Customer NIC :</th>
                  <th >Name:</th>
                  <th >Address:</th>
                  <th>Phone:</th>
                  <th >Edit:</th>
                </tr>
              </thead>
              <tbody style={{ display: 'block', overflowY: 'scroll', height: '500px' }}>
                {details}
              </tbody>

            </Table>
          </div>
        </Spin>
      </div>
    )
  }

}

export default ViewCustomer