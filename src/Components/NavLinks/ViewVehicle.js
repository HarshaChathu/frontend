import React, { Component } from 'react'
import axios from 'axios'
import Swal from 'sweetalert2'
import { Table, Button } from 'react-bootstrap'
import Background from '../../Images/background.jpg'
import { Spin } from "antd"
import "antd/dist/antd.css"
import 'bootstrap/dist/css/bootstrap.css';


//<Spin indicator={<LoadingOutlined style={{ fontSize: 30 }}/>}/>
//<Progress width="30px"  type="circle" percent={50} status="exception" />
//<Progress width="30px"  type="circle" percent={100} />


class ViewVehicle extends Component {
//assigning initial states for the values
  constructor(props) {
    super(props);
    this.state = {
      vehicledata: [],
      loading: false,
      userdata: JSON.parse(sessionStorage.getItem("usermail")),
    }
  }
//control the data when rendering this page
  componentDidMount() {
    var self = this
    //get vehicle data from the server for the user
    axios.get(`http://localhost:8401/task/getallvehiclesbyemail?email=${this.state.userdata}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ vehicledata: response.data });
          
        } else {
          self.setState({ loading: true });
          Swal.fire("No Vehicles for you!!", "", "error");
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ loading: true });
        Swal.fire("Error!", "Server Failed!", "error");
      });


  }



//render vehicle list on the table
  render() {
    //set the data to the rows in the table
    const details = this.state.vehicledata.map((vehicle, i) => (
      <tr key={i} style={{display:'table', width:'100%',tableLayout:'fixed'}}>
        {
          vehicle.map((num, j) =>
            <td key={j}>{num}</td>
          )
        }
        <td align="center">
          <Button onClick={() => {//from to update the vehicle data will be displayed
            (async () => {
              const { value: formValues } = await Swal.fire({
                title: `Vehicle : ${vehicle[0]}`,
                html: `<table className="viewTable" cellpadding="5" cellspacing="5">
                                    <tr>
                                      <td align="left">Reg No :</td>
                                      <td align="left" className="leftPad"><input value="${vehicle[0]}" id="swal-input1" class="swal2-input"></td>
                                    </tr>
                                    <tr>
                                      <td align="left">Model :</td>
                                      <td align="left" className="leftPad"><input value="${vehicle[1]}" id="swal-input2" class="swal2-input"></td>
                                    </tr>
                                    <tr>
                                      <td align="left">Make :</td>
                                      <td align="left" className="leftPad"><input value="${vehicle[2]}" id="swal-input3" class="swal2-input"></td>
                                    </tr>
                                    <tr>
                                      <td align="left">Milage :</td>
                                      <td align="left" className="leftPad"><input value="${vehicle[3]}" id="swal-input4" class="swal2-input"></td>
                                    </tr>
                                </table>`,
                focusConfirm: false,
                confirmButtonText: `Save`,
                showCancelButton: true,
                preConfirm: () => {
                  return [
                    document.getElementById("swal-input1").value,
                    document.getElementById("swal-input2").value,
                    document.getElementById("swal-input3").value,
                    document.getElementById("swal-input4").value
                  ];
                }
              });
            
              if (formValues) {
                //update the new vehicle data
                axios.post(`http://localhost:8401/vehicle/update?regNo=${formValues[0]}&make=${formValues[2]}&model=${formValues[1]}&milage=${formValues[3]}`)
                                    .then(res => {
                                        if (res.status === 201 || res.status === 200) {
                                            Swal.fire(
                                                'Updated!',
                                                'Vehicle data Updated',
                                                'success'
                                            )
                                            this.componentDidMount();//re render the table
                                        }
                                    })
                                    .catch(err => {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Server error occured',
                                            html: '<p>Try again in few minutes</p>'
                                        })
                                    });
              }
            })();
          }}>Edit</Button>
        </td>
      </tr>
    ))

    return (
      <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
        <div style={{ textAlign: "center", marginTop: "1.5rem", fontFamily: "sans-serif" }}><h1 style={{color:'white'}}>Vehicles</h1></div>
        <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>

          <div style={{ margin: "2rem" }}>
            <Table width="100%" bordered="true" variant="dark" >
              <thead style={{display:'table', width:'100%',tableLayout:'fixed'}}>
                <tr style={{ backgroundColor: "#9d46fa" }}>
                  <th >Reg No :</th>
                  <th >Model:</th>
                  <th>Make:</th>
                  <th >Current Milage in Km:</th>
                  <th >Edit:</th>
                </tr>
              </thead>
              <tbody style={{ display: 'block', overflowY: 'scroll', height: '500px' }}>
                {details}
              </tbody>

            </Table>
          </div>
        </Spin>
      </div>
    )
  }

}

export default ViewVehicle