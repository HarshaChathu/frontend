import React, { Component } from 'react'
import axios from 'axios'
import Swal from 'sweetalert2'
import { Table, Button } from 'react-bootstrap'
import Background from '../../Images/background.jpg'
import { Spin } from "antd"
import "antd/dist/antd.css"
import { Progress } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { Input } from "reactstrap"
import 'bootstrap/dist/css/bootstrap.css';


//<Spin indicator={<LoadingOutlined style={{ fontSize: 30 }}/>}/>
//<Progress width="30px"  type="circle" percent={50} status="exception" />
//<Progress width="30px"  type="circle" percent={100} />


class ViewTasks extends Component {
  //assigning initial states for the values
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      address: '',
      phone: '',
      nic: '',
      data: [],
      customerdata: [],
      loading: false,
      userdata: JSON.parse(sessionStorage.getItem("usermail")),
    }
  }
  //control the data when rendering this page
  componentDidMount() {
    var self = this
    //get ongoing tasks for the user
    axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.userdata}&activestatus=${0}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ data: response.data });
        } else {
          Swal.fire("No Data!", "", "error");
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ loading: true });
        Swal.fire("Error!", "Server Failed!", "error");
      });


  }
  //show status icon according to the active state of the task
  //0 means ongoing task - a rounded icon is spinning
  //1 means completed task - green check icon will display
  //2 is set for terminated tasks - red cross icon will display
  showStatus = (status) => {
    if (status === 0) {
      return (<Spin indicator={<LoadingOutlined style={{ fontSize: 30 }} />} />)
    }
    else if (status === 1) {
      return (<Progress width="30px" type="circle" percent={100} />)
    }
    else {
      return (<Progress width="30px" type="circle" percent={100} status="exception" />)
    }
  }

  //when changing show tasks by drop down this will called
  handleselect = (evt) => {
    const value = evt.target.value;
    this.setState({
      ...this.state,
      status: value
    });
    var self = this
    if (value === 'All') {

      self.setState({ data: [] });
      //show all tasks for the user
      axios.get(`http://localhost:8401/task/searchbyuseremail?email=${this.state.userdata}`)
        .then(function (response) {
          if (response.data.length > 0) {
            self.setState({ data: response.data });

          } else {
            self.setState({ loading: true });
            Swal.fire("No Data!", "", "error");
          }
        })
        .catch(function (error) {
          console.log(error);
          self.setState({ loading: true });
          Swal.fire("Error!", "Server Failed!", "error");
        });
    } else if (value === 'Terminated') {
      self.setState({ data: [] });
      //show terminated tasks for the user
      axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.userdata}&activestatus=${2}`)
        .then(function (response) {
          if (response.data.length > 0) {
            self.setState({ data: response.data });
          } else {
            Swal.fire("No Data!", "", "error");

          }
        })
        .catch(function (error) {
          console.log(error);
          self.setState({ loading: true });
          Swal.fire("Error!", "Server Failed!", "error");
        });
    } else if (value === 'Ongoing') {
      self.setState({ data: [] });
      //show ongoing tasks for the user
      axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.userdata}&activestatus=${0}`)
        .then(function (response) {
          if (response.data.length > 0) {
            self.setState({ data: response.data });
          } else {
            Swal.fire("No Data!", "", "error");
          }
        })
        .catch(function (error) {
          console.log(error);
          self.setState({ loading: true });
          Swal.fire("Error!", "Server Failed!", "error");
        });
    }
    else if (value === 'Completed') {
      self.setState({ data: [] });
      //show completed tasks for the user
      axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.userdata}&activestatus=${1}`)
        .then(function (response) {
          console.log(response.data);
          if (response.data.length > 0) {
            self.setState({ data: response.data });
          } else {
            Swal.fire("No Data!", "", "error");

          }
        })
        .catch(function (error) {
          console.log(error);
          self.setState({ loading: true });
          Swal.fire("Error!", "Server Failed!", "error");
        });
    }
  }
  //called when manage button clicked by the user
  //here the items can be added only for ongoing tasks, they can be terminated
  //if the task is completed or terminated they can be deleted
  //item.activestatus === 0 for ongoing tasks
  handleManage(item) {
    console.log(item.activestatus);
    if (item.activestatus === 0) {
      Swal.fire({
        showCancelButton: true,
        showDenyButton: true,
        confirmButtonText: `Add Items`,
        denyButtonText: `Terminate`,
        width: `45%`,
        title: 'Click Add Items to continue',
        text: "Note: Terminating the task cannot be revert",
      }).then((result) => {
        if (result.isConfirmed) {
          //pass the data to add items to the task page when add items clicked
          this.props.history.push({
            pathname: '/user/task/additems',
            state: { task: item }
          })
        }
        else if (result.isDenied) {
          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
          }).then((result) => {
            if (result.isConfirmed) {
              //to terminate the task
              axios.post(`http://localhost:8401/task/modifyactivestatus?taskId=${item.id}&email=${this.state.userdata}&regNo=${item.vehicle.regNo}`)
                .then(res => {
                  if (res.status === 201 || res.status === 200) {
                    Swal.fire(
                      'Terminated!',
                      'Task has been Terminated.',
                      'success'
                    ).then((result3) => {
                      if(result3.isConfirmed){
                        var self = this
                        if(this.state.status === 'All'){
                          self.setState({ data: [] });
                          //show all tasks for the user
                          axios.get(`http://localhost:8401/task/searchbyuseremail?email=${this.state.userdata}`)
                            .then(function (response) {
                              if (response.data.length > 0) {
                                self.setState({ data: response.data });

                              } else {
                                self.setState({ loading: true });
                                Swal.fire("No Data!", "", "error");
                              }
                            })
                            .catch(function (error) {
                              console.log(error);
                              self.setState({ loading: true });
                              Swal.fire("Error!", "Server Failed!", "error");
                            });
                        }
                        else{
                          this.componentDidMount();
                        }
                      }
                    })
                    
                  }
                })
                .catch(err => {
                  this.setState({ loading: true });
                  Swal.fire({
                    icon: 'error',
                    title: 'Server error occured',
                    html: '<p>Try again in few minutes</p>'
                  })
                });
            }
          })
        }
      })
    }
    else {
      Swal.fire({
        showCancelButton: true,
        showConfirmButton: false,
        showDenyButton: true,
        denyButtonText: `Delete`,
        width: `45%`,
        title: 'Delete Selected Task',
        text: "Note: Deleting the task cannot be revert",
      }).then((result) => {
        if (result.isDenied) {
          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
          }).then((result) => {
            if (result.isConfirmed) {
              //to delete the task
              axios.post(`http://localhost:8401/task/deletebytaskid?id=${item.id}`)
                .then(res => {
                  if (res.status === 201 || res.status === 200) {
                    Swal.fire(
                      'Deleted!',
                      'Task has been Deleted.',
                      'success'
                    ).then((result2) => {
                      if (result2.isConfirmed) {
                        var self = this
                        if (this.state.status === 'Completed') {
                          self.setState({ data: [] });
                          //show completed tasks for the user
                          axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.userdata}&activestatus=${1}`)
                            .then(function (response) {
                              if (response.data.length > 0) {
                                self.setState({ data: response.data });
                              } else {
                                Swal.fire("No Data!", "", "error");

                              }
                            })
                            .catch(function (error) {
                              console.log(error);
                              self.setState({ loading: true });
                              Swal.fire("Error!", "Server Failed!", "error");
                            });
                        }
                        else if (this.state.status === 'Terminated') {
                          self.setState({ data: [] });
                          //show terminated tasks for the user
                          axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.userdata}&activestatus=${2}`)
                            .then(function (response) {
                              if (response.data.length > 0) {
                                self.setState({ data: response.data });
                              } else {
                                Swal.fire("No Data!", "", "error");

                              }
                            })
                            .catch(function (error) {
                              console.log(error);
                              self.setState({ loading: true });
                              Swal.fire("Error!", "Server Failed!", "error");
                            });
                        }
                        else {
                          self.setState({ data: [] });
                          //show all tasks for the user
                          axios.get(`http://localhost:8401/task/searchbyuseremail?email=${this.state.userdata}`)
                            .then(function (response) {
                              if (response.data.length > 0) {
                                self.setState({ data: response.data });

                              } else {
                                self.setState({ loading: true });
                                Swal.fire("No Data!", "", "error");
                              }
                            })
                            .catch(function (error) {
                              console.log(error);
                              self.setState({ loading: true });
                              Swal.fire("Error!", "Server Failed!", "error");
                            });
                        }
                      }
                    })

                  }
                })
                .catch(err => {

                  this.setState({ loading: true });
                  Swal.fire({
                    icon: 'error',
                    title: 'Server error occured',
                    html: '<p>Try again in few minutes</p>'
                  })
                });
            }
          })
        }
      })
    }
  }
  //render the task table
  render() {
    //set the data for the rows in the table
    const details = this.state.data.map((item) => (
      <tr style={{ display: 'table', tableLayout: 'fixed', width: '100%' }}>
        <td >{item.vehicle.regNo}</td>
        <td >{item.vehicle.make}</td>
        <td >{item.vehicle.model}</td>
        <td align="center">{this.showStatus(item.activestatus)}</td>
        <td align="center"><Button variant="info" onClick={() => {
          Swal.fire({
            title: `Customer : ${item.customer.name}`,
            html: `<table className="viewTable" cellpadding="5" cellspacing="5">
                                    <tr>
                                      <td align="left">NIC :</td>
                                      <td align="left" className="leftPad">${item.customer.nic}</td>
                                    </tr>
                                    <tr>
                                      <td align="left">Address :</td>
                                      <td align="left" className="leftPad">${item.customer.address}</td>
                                    </tr>
                                    <tr>
                                      <td align="left">Phone :</td>
                                      <td align="left" className="leftPad">${item.customer.phoneNo}</td>
                                    </tr>
                                </table>`,
          })

        }}>Show</Button></td>
        <td align="center"><Button variant="info" onClick={() => {
          Swal.fire({
            title: `Vehicle : ${item.vehicle.regNo}`,
            html: `<table className="viewTable" cellpadding="5" cellspacing="5">
                                    <tr>
                                      <td align="left">Milage :</td>
                                      <td align="left" className="leftPad">${item.vehicle.milage} Km</td>
                                    </tr>
                                    <tr>
                                      <td align="left">Service Type :</td>
                                      <td align="left" className="leftPad">${item.servicetype}</td>
                                    </tr>
                                    <tr>
                                      <td align="left">Additional Info :</td>
                                      <td align="left" className="leftPad">${item.additionalinfo}</td>
                                    </tr>
                                    <tr>
                                      <td align="left">Cost : (Rs.)</td>
                                      <td align="left" className="leftPad">${item.totalCost}</td>
                                    </tr>
                                    <tr>
                                      <td align="left">Completed at :</td>
                                      <td align="left" className="leftPad">${item.completedDate}</td>
                                    </tr>
                                    <tr>
                                      <td align="left">Added at :</td>
                                      <td align="left" className="leftPad">${item.addedDate}</td>
                                    </tr>
                                </table>`,
          })
        }}>Show</Button>{' '}</td>
        <td align="center">
          <Button onClick={() => { this.handleManage(item) }}>Manage</Button>
        </td>
      </tr>
    ))

    return (
      <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
        <div style={{ textAlign: "center", marginTop: "1.5rem", fontFamily: "sans-serif" }}><h1 style={{ color: 'white' }}>Task List</h1></div>
        <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>
          <div style={{ margin: "2rem" }}>
            <Table width="40%" bordered="true" variant="dark" >
              <tbody>
                <tr width="40%">
                  <td width="10%">Show Tasks By: </td>
                  <td width="30%">
                    <Input type="select" name="statusselect" id="statusselect" onChange={this.handleselect}>
                      <option >Ongoing</option>
                      <option>Completed</option>
                      <option>Terminated</option>
                      <option>All</option>
                    </Input>
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
          <div style={{ margin: "2rem" }}>
            <Table width="100%" bordered="true" variant="dark" >
              <thead style={{ display: 'table', tableLayout: 'fixed', width: '100%' }}>
                <tr style={{ backgroundColor: "#9d46fa" }}>
                  <th >Vehicle Reg No:</th>
                  <th >Make:</th>
                  <th >Model:</th>
                  <th >Status:</th>
                  <th >Customer Info:</th>
                  <th >Other Details</th>
                  <th ></th>
                </tr>
              </thead>
              <tbody style={{ display: 'block', overflowY: 'scroll', height: '500px' }}>
                {details}
              </tbody>

            </Table>
          </div>
        </Spin>
      </div>
    )
  }

}

export default ViewTasks