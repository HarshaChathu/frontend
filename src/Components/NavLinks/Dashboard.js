import React, { Component } from 'react';
import { Spin } from "antd";
import Background from '../../Images/background.jpg'
import { Card, Table, Button } from 'react-bootstrap'
import { LoadingOutlined } from '@ant-design/icons';
import { Progress } from 'antd';
import axios from 'axios'
import Swal from 'sweetalert2'
import { PushpinOutlined,CheckSquareOutlined,CheckCircleOutlined, AlertOutlined, UsergroupDeleteOutlined, CarOutlined, DollarCircleOutlined } from '@ant-design/icons';

class Dashboard extends Component {

  //assigning initial states for the values
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      usermail: JSON.parse(sessionStorage.getItem("usermail")),//get saved user email at sign in page
      userservicetype: JSON.parse(sessionStorage.getItem("userservicetype")),//get saved user service type at sign in page
      alltasks: '',
      completedtasks: '',
      ongointasks: '',
      terminatedtasks: '',
      allcustomers: '',
      allvehicles: '',
      totalearnings: ''
    }
  }
  //control the data when rendering this page
  componentDidMount() {
    var self = this

    if(self.state.userservicetype === 'Repair Center'){
      //get most repaired vehicle from the database fro repair centers
    axios.get(`http://localhost:8401/task/getallrepairedvehiclesforrepair?email=${this.state.usermail}`)
    .then(function (response) {
      if(response.data[0] === undefined){
        self.setState({ mostrepairedvehicle: 'Unavailable' });
      }
      else{
        self.setState({ mostrepairedvehicle: response.data[0][1] });
      }
    })
    .catch(function (error) {
      console.log(error);
      Swal.fire("Error!", "Server Failed!", "error");
    });
    }



    if(self.state.userservicetype === 'Both'){
       //get most repaired vehicle from the database for centers using both
    axios.get(`http://localhost:8401/task/getallrepairedvehiclesforboth?email=${this.state.usermail}`)
    .then(function (response) {
      if(response.data[0] === undefined){
        self.setState({ mostrepairedvehicleforboth: 'Unavailable' });
      }
      else{
        self.setState({ mostrepairedvehicleforboth: response.data[0][1] });
      }
    })
    .catch(function (error) {
      console.log(error);
      Swal.fire("Error!", "Server Failed!", "error");
    });
    }


    //get most used item from the database
    axios.get(`http://localhost:8401/taskitems/getmostuseditemuseremail?email=${this.state.usermail}`)
      .then(function (response) {
        if(response.data[0] === undefined){
          self.setState({ mostuseditem: 'Unavailable' });
        }
        else{
          self.setState({ mostuseditem: response.data[0][1] });
        }
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire("Error!", "Server Failed!", "error");
      });
    //get user's center title from the database
    axios.get(`http://localhost:8401/user/searchemail?email=${this.state.usermail}`)
      .then(function (response) {
        if (response.data.email.length > 0) {

          self.setState({ usertitle: response.data.title });
        }
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire("Error!", "Server Failed!", "error");
      });


      //get all completed tasks for current month
    axios.get(`http://localhost:8401/task/getcurrentmonthcompletedtasks?email=${this.state.usermail}`)
    .then(function (response) {
      if (response.data.length > 0) {
        var sum = 0;
        for (var x = 0; x < response.data.length; x++) {
          sum += response.data[x].task.totalCost;
        }
        self.setState({ currentmonthearnings: 'Rs. '+sum });
      } else {
        self.setState({ currentmonthearnings: 'None' });
      }
    })
    .catch(function (error) {
      console.log(error);
      Swal.fire("Error!", "Server Failed!", "error");
    });

     //get all completed tasks for previous month
     axios.get(`http://localhost:8401/task/getpreviousmonthcompletedtasks?email=${this.state.usermail}`)
     .then(function (response) {
       if (response.data.length > 0) {
         var sum = 0;
         for (var x = 0; x < response.data.length; x++) {
           sum += response.data[x].task.totalCost;
         }
         self.setState({ previousmonthearnings: 'Rs. '+ sum });
       } else {
         self.setState({ previousmonthearnings: 'None' });
       }
     })
     .catch(function (error) {
       console.log(error);
       Swal.fire("Error!", "Server Failed!", "error");
     });

     //get all services came for the center
    axios.get(`http://localhost:8401/task/getallvehicleservicetasksbyemail?email=${this.state.usermail}`)
    .then(function (response) {
      if (response.data.length > 0) {
        self.setState({ allservices: response.data.length });
      } else {
        self.setState({ allservices: response.data.length });
      }
    })
    .catch(function (error) {
      console.log(error);
      Swal.fire("Error!", "Server Failed!", "error");
    });

    //get all repairs came for the center
    axios.get(`http://localhost:8401/task/getallvehiclerepairtasksbyemail?email=${this.state.usermail}`)
    .then(function (response) {
      if (response.data.length > 0) {
        self.setState({ allrepairs: response.data.length });
      } else {
        self.setState({ allrepairs: response.data.length });
      }
    })
    .catch(function (error) {
      console.log(error);
      Swal.fire("Error!", "Server Failed!", "error");
    });

    //earnings for all services
    axios.get(`http://localhost:8401/task/getallcompletedvehicleservicetasksbyemail?email=${this.state.usermail}`)
    .then(function (response) {
      if(response.data.length>0){
        var sum=0;
        for (var x = 0; x < response.data.length; x++) {
          sum += response.data[x].task.totalCost;
        }
        self.setState({ totalearningsforservices: 'Rs. '+sum });
      }
      else{
        self.setState({ totalearningsforservices: 'None' });
      }
    })
    .catch(function (error) {
      console.log(error);
      Swal.fire("Error!", "Server Failed!", "error");
    });

    //earnings for all repairs
    axios.get(`http://localhost:8401/task/getallcompletedvehiclerepairtasksbyemail?email=${this.state.usermail}`)
    .then(function (response) {
      if(response.data.length>0){
        var sum=0;
        for (var x = 0; x < response.data.length; x++) {
          sum += response.data[x].task.totalCost;
        }
        self.setState({ totalearningsforrepairs: 'Rs. '+sum });
      }
      else{
        self.setState({ totalearningsforrepairs: 'None' });
      }
    })
    .catch(function (error) {
      console.log(error);
      Swal.fire("Error!", "Server Failed!", "error");
    });

      //get all tasks for a user and calculate total earnings
    axios.get(`http://localhost:8401/task/searchbyuseremail?email=${this.state.usermail}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ alltasks: response.data.length });
          self.setState({ loading: false });
          var sum = 0;
          for (var x = 0; x < response.data.length; x++) {
            sum += response.data[x].totalCost;
          }
          self.setState({ totalearnings: 'Rs. '+sum });
        } else {
          self.setState({ totalearnings: 'None' });
          self.setState({ alltasks: response.data.length });
        }
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire("Error!", "Server Failed!", "error");
      });

      //get terminated tasks for a user
    axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.usermail}&activestatus=${2}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ terminatedtasks: response.data.length });
          self.setState({ loading: false });
        } else {
          self.setState({ terminatedtasks: response.data.length });
        }
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire("Error!", "Server Failed!", "error");
      });
      //get completed tasks for a user
    axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.usermail}&activestatus=${1}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ completedtasks: response.data.length });
          self.setState({ loading: false });
        } else {
          self.setState({ completedtasks: response.data.length });
        }
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire("Error!", "Server Failed!", "error");
      });
      //get ongoing tasks for a user
    axios.get(`http://localhost:8401/task/searchbyuseremailandactivestatus?email=${this.state.usermail}&activestatus=${0}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ ongointasks: response.data.length });
          self.setState({ loading: false });
        } else {
          self.setState({ ongointasks: response.data.length });
        }
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire("Error!", "Server Failed!", "error");
      });

      //get all customers for a user
    axios.get(`http://localhost:8401/task/getallcustomersbyemail?email=${this.state.usermail}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ allcustomers: response.data.length });
          self.setState({ loading: false });
        } else {
          self.setState({ allcustomers: response.data.length });
        }
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire("Error!", "Server Failed!", "error");
      });
      //get all vehicles for a user
    axios.get(`http://localhost:8401/task/getallvehiclesbyemail?email=${this.state.usermail}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ allvehicles: response.data.length });
          self.setState({ loading: false });
        } else {
          self.setState({ allvehicles: response.data.length });
        }
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire("Error!", "Server Failed!", "error");
      });
  }

  //change rendering method when the user's service type changed
  setForUserServiceType = () => {
    //for a user who used both service center and repair center facilities
    if (this.state.userservicetype === "Both") {
      return (
        <Table width="100%" borderless>
          <tbody>
            <tr width="100%">
              <td align="center" width="20%">
                <Card style={{ height: '11rem' }}>
                  <CheckCircleOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                  <Card.Body>
                    <Card.Title style={{ textAlign: "center" }}>Total Services</Card.Title>
                    <Card.Title style={{ textAlign: "center" }}>{this.state.allservices}</Card.Title>
                  </Card.Body>
                </Card>
              </td>
              <td align="center" width="20%">
                <Card style={{ height: '11rem' }}>
                  <DollarCircleOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                  <Card.Body>
                    <Card.Title style={{ textAlign: "center" }}>Earnings from Services</Card.Title>
                    <Card.Title style={{ textAlign: "center" }}>{this.state.totalearningsforservices}</Card.Title>
                  </Card.Body>
                </Card>
              </td>
              <td align="center" width="20%">
                <Card style={{ height: '11rem' }}>
                  <CheckSquareOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                  <Card.Body>
                    <Card.Title style={{ textAlign: "center" }}>Total Repairs</Card.Title>
                    <Card.Title style={{ textAlign: "center" }}>{this.state.allrepairs}</Card.Title>
                  </Card.Body>
                </Card>
              </td>
              <td align="center" width="20%">
                <Card style={{ height: '11rem' }}>
                  <DollarCircleOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                  <Card.Body>
                    <Card.Title style={{ textAlign: "center" }}>Earnings from Repairs</Card.Title>
                    <Card.Title style={{ textAlign: "center" }}>{this.state.totalearningsforrepairs}</Card.Title>
                  </Card.Body>
                </Card>
              </td>
              <td align="center" width="20%">
                <Card style={{ height: '11rem' }}>
                  <PushpinOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                  <Card.Body>
                    <Card.Title style={{ textAlign: "center" }}>Most Repaired Vehicle</Card.Title>
                    <Card.Title style={{ textAlign: "center" }}>{this.state.mostrepairedvehicleforboth}</Card.Title>
                  </Card.Body>
                </Card>
              </td>
            </tr>

          </tbody>
          <tbody>
          <tr width="50%"> 
              <td colspan="5" align="center"><Button variant="success" href="/user/task/viewinvoices">View My Reports</Button></td>
            </tr>
          </tbody>
        </Table>
      )
    }
    //for a user who used repair center facilities
    else if (this.state.userservicetype === "Repair Center") {
      return (
        <Table width="100%" borderless>
          <tbody>
            <tr width="50%">
              <td align="center" style={{fontSize:'20px' ,color:'white', fontWeight:'30'}}>
                Most Repaired Vehicle :- {this.state.mostrepairedvehicle}
              </td>
            </tr>
            <tr width="50%"> 
              <td align="center"><Button variant="success" href="/user/task/viewinvoices">View My Reports</Button></td>
            </tr>
          </tbody>
        </Table>
      )
    }
    else {//for a user who used service center facilities
      return (
        <Table width="100%" borderless>
          <tbody>
          <tr width="50%"> 
              <td align="center"><Button variant="success" href="/user/task/viewinvoices">View My Reports</Button></td>
            </tr>
          </tbody>
        </Table>
      )
    }
  }


  render() {{/*Render the dashboard componet*/}
    return (
      <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
        <div style={{ textAlign: "center", marginTop: "1.5rem", fontFamily: "sans-serif" }}><h2 style={{color:'white'}}>{this.state.usertitle}</h2></div>
        <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>
          <div style={{ marginTop: "0.5rem" }}>
            <Table width="100%" borderless>
              <tbody>
                <tr width="100%">
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <Progress width="60px" type="circle" percent={100} style={{ marginTop: "1rem" }} format={() => 'All'} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>All Tasks</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.alltasks}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <Progress width="60px" type="circle" percent={100} style={{ marginTop: "1rem" }} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>Completed Tasks</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.completedtasks}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <Spin indicator={<LoadingOutlined style={{ fontSize: 60, marginTop: "1rem" }} />} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>Ongoing Tasks</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.ongointasks}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <Progress width="60px" type="circle" percent={100} status="exception" style={{ marginTop: "1rem" }} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>Terminated Tasks</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.terminatedtasks}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <AlertOutlined style={{ fontSize: '60px', color: '#08c', marginTop: "1rem" }} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>Most Used Item</Card.Title>
                         <Card.Title style={{ textAlign: "center" }}>{this.state.mostuseditem}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                </tr>
              </tbody>
            </Table>
            <Table width="100%" borderless>
              <tbody>
                <tr width="100%">
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <UsergroupDeleteOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>Total Customers</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.allcustomers}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <CarOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>Total Vehicles</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.allvehicles}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <DollarCircleOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>Total Earnings</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.totalearnings}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <DollarCircleOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>This Month Earnings</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.currentmonthearnings}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                  <td align="center" width="20%">
                    <Card style={{ height: '11rem' }}>
                      <DollarCircleOutlined style={{ fontSize: '50px', color: '#08c', marginTop: "1rem" }} />
                      <Card.Body>
                        <Card.Title style={{ textAlign: "center" }}>Last Month Earnings</Card.Title>
                        <Card.Title style={{ textAlign: "center" }}>{this.state.previousmonthearnings}</Card.Title>
                      </Card.Body>
                    </Card>
                  </td>
                </tr>

              </tbody>
            </Table>
          </div>
          <div>
            {this.setForUserServiceType()}
          </div>
        </Spin>
      </div>
    );
  }
}

export default Dashboard;