import React from 'react';
import Background from '../../Images/background.jpg'
import { Spin, Select, Input } from "antd";
import { Table, Col, Row, Container, Button, Card, ListGroup } from 'react-bootstrap'
import axios from 'axios'
import Swal from 'sweetalert2'
import "antd/dist/antd.css";
import jsPDF from "jspdf";
import "jspdf-autotable";

class AddItemsToTask extends React.Component {
    //assigning initial states for the values
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            total: 0,
            categorylist: [],
            itemlist: [],
            addeditemlist: [],
            inputvalues: {
                price: '',
                quantity: ''
            },
            itemname: [],
            categoryname : [],
            userdata: JSON.parse(sessionStorage.getItem("usermail"))
        }
        this.onChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    //control the data when rendering this page
    componentDidMount() {
        var self = this
        //get user's category list from the database
        axios.get(`http://localhost:8401/category/getAllByUserEmail?email=${this.state.userdata}`)
            .then(function (response) {
                if (response.data.length > 0) {
                    self.setState({ categorylist: response.data });
                    self.setState({ loading: false });
                    self.getAddedItemList();//call and render added items from server
                } else {
                    Swal.fire("No Categories Found!", "", "error");
                }
            })
            .catch(function (error) {
                console.log(error);
                Swal.fire("Error!", "Server Failed!", "error");
            });
        //get user's center's title from the server
        axios.get(`http://localhost:8401/user/searchemail?email=${this.state.userdata}`)
            .then(function (response) {
                console.log(response.data.email.length);
                if (response.data.email.length > 0) {

                    self.setState({ usertitle: response.data.title });
                }
            })
            .catch(function (error) {
                console.log(error);
                self.setState({ loading: true });
                Swal.fire("Error!", "Server Failed!", "error");
            });
    }

    //get added items list from the server for the current task
    getAddedItemList = () => {
        var self = this
        //get added items and calculate total
        axios.get(`http://localhost:8401/taskitems/getallbytaskidanduseremail?taskId=${this.props.location.state.task.id}&email=${this.state.userdata}`)
            .then(function (response) {
                if (response.data.length > 0) {
                    self.setState({ addeditemlist: response.data });
                    self.setState({ loading: false });
                    var sum = 0;
                    for (var x = 0; x < response.data.length; x++) {
                        sum += response.data[x].price;
                    }
                    self.setState({ total: sum });
                } else {
                    self.setState({ addeditemlist: [] });
                    self.setState({ total: 0 });
                }
            })
            .catch(function (error) {
                console.log(error);
                Swal.fire("Error!", "Server Failed!", "error");
            });
    }

    onItemChange(value) {
        console.log(`selected ${value}`);
        this.setState({
            itemname: value
        });
    }
    //called when selecting category from the drop down and get items for the selected category
    onCategoryChange(value) {
        var self = this
        self.setState({
            categoryname: value
        });
        //get items for the category
        axios.get(`http://localhost:8401/item/getallitemsbycategoryandemail?email=${this.state.userdata}&category=${value}`)
            .then(function (response) {
                if (response.data.length > 0) {
                    self.setState({ itemlist: response.data });
                }
                else {
                    self.setState({ itemlist: response.data });
                }
            })
            .catch(function (error) {
                console.log(error);
                Swal.fire("Error!", "Server Failed!", "error");
            });
    }
    //called to do the actions for the invoice
    generatePDF(completedDate) {
        var self = this;
        const doc = new jsPDF();
        //setting the column names 
        const tableColumn = ["Item name", "Quantity", "Price (Rs.)"];
        const tableRows = [];

        //get page width
        var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

        //setting table row data
        this.state.addeditemlist.forEach(item => {
            const itemData = [
                item.itemName,
                item.quantity,
                item.price
            ];
            // push each item's info into a row
            tableRows.push(itemData);
        });
        // startY is basically margin-top
        doc.autoTable(tableColumn, tableRows, { startY: 40 });
        //setting up the document interface
        doc.text("AUTOWORKS - " + this.state.usertitle, pageWidth / 2, 10, 'center');
        doc.setFontSize(10);
        doc.text('Customer Name - ' + this.props.location.state.task.customer.name, 14, 20);
        doc.text('Customer Phone - ' + this.props.location.state.task.customer.phoneNo, 14, 25);
        doc.text('Added At - ' + this.props.location.state.task.addedDate, 14, 30);
        doc.text('Completed At - ' + completedDate, 14, 35);

        doc.text('Vehicle Reg.No. - ' + this.props.location.state.task.vehicle.regNo, 145, 20);
        doc.text('Vehicle - ' + this.props.location.state.task.vehicle.make + ' ' + this.props.location.state.task.vehicle.model, 145, 25);
        doc.text('Milage - ' + this.props.location.state.task.vehicle.milage + 'Km', 145, 30);
        doc.text('Total Cost - Rs.' + this.state.total + '.00', 145, 35);
        //define the name of invoice PDF file.
        //doc.save('invoice.pdf');
            var data = new FormData();
            data.append('file', doc.output('blob'),self.props.location.state.task.id);
            data.append('taskId', self.props.location.state.task.id);
            data.append('email', self.state.userdata);
            data.append('regNo', this.props.location.state.task.vehicle.regNo);
            data.append('customerName', this.props.location.state.task.customer.name);
            data.append('completedDate', completedDate);
            //send pdf data to server
            axios.post(`http://localhost:8401/task/savecompleteddoc`, data)
                .then(res => {
                    if (res.status === 201 || res.status === 200) {
                        doc.save('Invoice-'+this.props.location.state.task.vehicle.regNo);
                        window.location.href='/user/task/listtasks'
                    }
                })
                .catch(err => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Server error occured',
                        html: '<p>Try again in few minutes</p>'
                    })
                });

    }
    //need to get the completed date time from server after changing active status of the task
    refreshCompletedDate = () => {
        var self = this;
        axios.get(`http://localhost:8401/task/searchbytaskid?taskId=${this.props.location.state.task.id}`)
            .then(function (response) {
                if (response.data.addedDate.length > 0) {
                    self.generatePDF(response.data.completedDate);

                }
            })
            .catch(function (error) {
                console.log(error);
                Swal.fire("Error!", "Server Failed!", "error");
            });
    }
    //called when process button clicked to finish the task
    handleProcess(event) {
        if(this.state.addeditemlist.length>0){
            //modify active status of the task to completed
        axios.post(`http://localhost:8401/task/modifyactivestatusonprocess?taskId=${this.props.location.state.task.id}&total=${this.state.total}`)
        .then(res => {
            if (res.status === 201 || res.status === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Task Completed!',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.refreshCompletedDate();//called to get the completed time and date
                    }
                })

            }
        })
        .catch(err => {
            Swal.fire({
                icon: 'error',
                title: 'Server error occured',
                html: '<p>Try again in few minutes</p>'
            })
        });
        }
        else{
            Swal.fire({
                icon: 'error',
                title: 'Add items before process'
            })
        }


    }
    //adding the item and it's details to the task
    handleSubmit(event) {
        event.preventDefault();
        console.log(this.props.location.state.task.id);
        console.log(this.state.userdata);
        console.log(this.state.itemname);
        console.log(this.state.inputvalues.price);
        console.log(this.state.inputvalues.quantity);
        if(!this.state.itemname || !this.state.inputvalues.price || !this.state.inputvalues.quantity){
            Swal.fire(
                'Fill the fields!',
                '',
                'error'
            )
        }
        else{
            //send details to the server
        axios.post(`http://localhost:8401/taskitems/create?taskId=${this.props.location.state.task.id}&email=${this.state.userdata}&itemname=${this.state.itemname}&quantity=${this.state.inputvalues.quantity}&price=${this.state.inputvalues.price}`)
        .then(res => {
            if (res.status === 201 || res.status === 200) {
                //console.log(res);
                Swal.fire({
                    icon: 'success',
                    title: 'Item Added!',
                })
                this.handleReset();//reset fields
                this.getAddedItemList();//referesh the added items list
            } 
        })
        .catch(err => {
            Swal.fire({
                icon: 'error',
                title: 'Server error occured',
                html: '<p>Try again in few minutes</p>'
            })
        });
        }

        
    }

    //reset fileds
    handleReset = () => {
        this.setState({
            categoryname: [],
            itemname: [],
            inputvalues: {
                price: '',
                quantity: ''
            },
        });
    }
    //called to change the states of the inputs
    handleChange(e) {
        var oldState = this.state.inputvalues;
        var newState = { [e.target.name]: e.target.value };

        this.setState({ inputvalues: Object.assign(oldState, newState) });
    }
    //render the Add Items To Task Page
    render() {

        const { Option } = Select;

        //load item list to the dropdown
        const loadeditems = this.state.itemlist.map((items) => (
            <Option key={items.name} value={items.name}>{items.name}</Option>
        ))
        //load categories for the drop down
        const loadcategories = this.state.categorylist.map((items) => (
            <Option key={items.categoryName} value={items.categoryName}>{items.categoryName}</Option>
        ))

        //load added item list in the table
        const addedItemList = this.state.addeditemlist.map((addeditems) => (
            <tr style={{display:'table', width:'100%',tableLayout:'fixed'}}>
                <td>{addeditems.itemName}</td>
                <td align="center">{addeditems.quantity}</td>
                <td align="right">{addeditems.price}</td>
                <td align="center"><Button variant="danger" size="sm" onClick={() => {
          Swal.fire({
            showCancelButton: true,
            showDenyButton: true,
            title: `Delete item ${addeditems.itemName}?`,
            icon: 'warning',
            showConfirmButton: false,
            denyButtonText: "Delete",
            width: `40%`,
          }).then((result) => {
            if (result.isDenied) {
              //delete selected item on delete button pressed
              axios.post(`http://localhost:8401/taskitems/deletebytaskitemid?id=${addeditems.id}`)
              .then(res => {
                  if (res.status === 201 || res.status === 200) {
                      
                      this.getAddedItemList();//re render the table
                  }
              })
              .catch(err => {
                  Swal.fire({
                      icon: 'error',
                      title: 'Server error occured',
                      html: '<p>Try again in few minutes</p>'
                  })
              });
            }
          })
        }}>Delete</Button></td>
            </tr>
        ))

        return (
            <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
                <div style={{ textAlign: "center", marginTop: "1.5rem", marginBottom: '1rem', fontFamily: "sans-serif" }}><h1 style={{ color: 'white' }}>Add Items to the task</h1></div>
                <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>

                    <Card border="primary" style={{ width: "95%", height: "100%", marginBottom: "1rem", margin: "auto", padding: '0.2rem' }}>
                        <Card.Body>
                            <Container>
                                <Row>
                                    <Col xs={6} md={4}>

                                        <Card bg="dark" border="info" style={{ width: '18rem' }}>
                                            <Card.Header style={{ color: 'white' }}>Customer Details</Card.Header>
                                            <ListGroup variant="flush">
                                                <ListGroup.Item>Name : {this.props.location.state.task.customer.name}</ListGroup.Item>
                                                <ListGroup.Item>Address : {this.props.location.state.task.customer.address}</ListGroup.Item>
                                                <ListGroup.Item>Mobile : {this.props.location.state.task.customer.phoneNo}</ListGroup.Item>
                                                <ListGroup.Item>NIC : {this.props.location.state.task.customer.nic}</ListGroup.Item>
                                            </ListGroup>
                                        </Card>
                                        <Card bg="dark" border="info" style={{ width: '18rem', marginTop: '2rem' }}>
                                            <Card.Header style={{ color: 'white' }}>Vehicle Details</Card.Header>
                                            <ListGroup variant="flush">
                                                <ListGroup.Item>Reg No : {this.props.location.state.task.vehicle.regNo}</ListGroup.Item>
                                                <ListGroup.Item>Make : {this.props.location.state.task.vehicle.make}</ListGroup.Item>
                                                <ListGroup.Item>Model : {this.props.location.state.task.vehicle.model}</ListGroup.Item>
                                                <ListGroup.Item>Milage : {this.props.location.state.task.vehicle.milage} Km</ListGroup.Item>
                                            </ListGroup>
                                        </Card>
                                        <Card bg="dark" border="info" style={{ width: '18rem', marginTop: '2rem' }}>
                                            <Card.Header style={{ color: 'white' }}>Other Details</Card.Header>
                                            <ListGroup variant="flush">
                                                <ListGroup.Item>Service Type : {this.props.location.state.task.servicetype}</ListGroup.Item>
                                                <ListGroup.Item>Added At : {this.props.location.state.task.addedDate}</ListGroup.Item>
                                                <ListGroup.Item>Additional Info : {this.props.location.state.task.additionalinfo}</ListGroup.Item>
                                                <ListGroup.Item>Milage : {this.props.location.state.task.vehicle.milage} Km</ListGroup.Item>
                                            </ListGroup>
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={8}>
                                        <Card border="primary" style={{ width: "100%", height: "100%", margin: "auto", padding: '0.2rem' }}>
                                            <Card.Body>
                                                <Table borderless>

                                                    <thead>
                                                        <tr align="center">
                                                            <th colspan="2">Add Items Here</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr  >
                                                            <td style={{ border: 'none' }}>
                                                                <Select
                                                                    size="large"
                                                                    showSearch
                                                                    value={this.state.categoryname}
                                                                    style={{ width: 325 }}
                                                                    placeholder="Select a category"
                                                                    optionFilterProp="children"
                                                                    onChange={this.onCategoryChange.bind(this)}
                                                                    filterOption={(input, option) =>
                                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                                    }
                                                                >
                                                                    {loadcategories}
                                                                </Select>
                                                            </td>
                                                            <td style={{ border: 'none' }}>
                                                                <Select
                                                                    size="large"
                                                                    showSearch
                                                                    value={this.state.itemname} 
                                                                    style={{ width: 325 }}
                                                                    placeholder="Select a item"
                                                                    optionFilterProp="children"
                                                                    onChange={this.onItemChange.bind(this)}
                                                                    filterOption={(input, option) =>
                                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                                    }
                                                                >
                                                                    {loadeditems}
                                                                </Select>
                                                            </td>


                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <Input size="large" placeholder="Enter Quantity/ Units" id="quantity" name="quantity" value={this.state.inputvalues.quantity} onChange={this.onChange} />
                                                            </td>
                                                            <td>
                                                                <Input size="large" type="number" placeholder="Enter Total Price (Rs.)" id="price" name="price" value={this.state.inputvalues.price} onChange={this.onChange} />
                                                            </td>
                                                        </tr>
                                                        <tr >

                                                            <td style={{ border: 'none' }}><Button style={{ width: 70 }} onClick={this.handleSubmit} variant="primary" size="sm">Add</Button></td>
                                                            <td style={{ border: 'none' }}><Button style={{ width: 70 }} onClick={this.handleReset} variant="primary" size="sm">Reset</Button></td>
                                                        </tr>
                                                    </tbody>
                                                </Table>

                                                <Table>
                                                    <thead style={{display:'table', width:'100%',tableLayout:'fixed'}}>
                                                        <tr align="center">
                                                            <th colspan="4">Added Items</th>
                                                        </tr>
                                                        <tr >
                                                            <th>Item name</th>
                                                            <th>Quantity</th>
                                                            <th>Price (Rs.)</th>
                                                            <th>Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style={{ display: 'block', overflowY: 'scroll', height: '300px' }}>

                                                        {addedItemList}

                                                    </tbody>
                                                    <tfoot style={{display:'table', width:'100%',tableLayout:'fixed'}}>
                                                        <tr>
                                                            <th colspan="2">Total</th>
                                                            <td align="right">{this.state.total}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <Button style={{ width: 70 }} variant="success" onClick={this.handleProcess.bind(this)} size="sm">Process</Button>
                                                                <Button style={{ width: 70, marginLeft: "2rem" }} href="/user/task/listtasks" variant="info" size="sm">Back</Button>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </Table>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>

                            </Container>
                        </Card.Body>
                    </Card>
                </Spin>
            </div>
        );
    }
}

export default AddItemsToTask;