import React, { Component } from 'react';
import { Form, Col, Button, Card, FormControl } from 'react-bootstrap'
import axios from 'axios'
import Swal from 'sweetalert2'
import { Spin } from "antd";
import "antd/dist/antd.css";
import Background from '../../Images/background.jpg';

class AddNewTask extends Component {
//assigning initial states for the values
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      address: '',
      phone: '',
      nic: '',
      regNo: '',
      make: '',
      model: '',
      milage: '',
      nicsearchtext: '',
      regnosearchtext: '',
      customerdata: [],
      vehicledata: [],
      additionalinfo: '',
      usermail: JSON.parse(sessionStorage.getItem("usermail")),
      loading: false,
      errors: {},
      servicetype: ''
    }
  }
//reset the fields
  handleReset = () => {
    this.setState({
      name: '',
      address: '',
      phone: '',
      nic: '',
      regNo: '',
      make: '',
      model: '',
      milage: '',
      nicsearchtext: '',
      regnosearchtext: '',
      additionalinfo: '',
      servicetype: '',
      errors: {}
    });
  };
//control the data when rendering this page
  componentDidMount(){
    var self = this
      //get user's service type from the server
        axios.get(`http://localhost:8401/user/searchemail?email=${this.state.usermail}`)
        .then(function (response) {
          console.log(response.data.email.length);
            if (response.data.email.length > 0) {
              
                self.setState({ userservicetype: response.data.servicetype });
                if(response.data.servicetype === 'Repair Center'){
                  self.setState({ servicetype: 'Vehicle Repair' });
                }
                if(response.data.servicetype === 'Service Center'){
                  self.setState({ servicetype: 'Vehicle Service' });
                }
            }
        })
        .catch(function (error) {
            console.log(error);
            self.setState({ loading: true });
            Swal.fire("Error!", "Server Failed!", "error");
        });
  }

  //called when inputs are chanigng
  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
    this.setState({ [target.address]: target.value });
    this.setState({ [target.phone]: target.value });
    this.setState({ [target.nic]: target.value });
    this.setState({ [target.regNo]: target.value });
    this.setState({ [target.make]: target.value });
    this.setState({ [target.model]: target.value });
    this.setState({ [target.milage]: target.value });
    this.setState({ [target.nicsearchtext]: target.value });
    this.setState({ [target.regnosearchtext]: target.value });
    this.setState({ [target.additionalinfo]: target.value });
  }
//called to search customer details by nic number
  searchByNic = event => {
    event.preventDefault();
    if (this.validatenicsearchtext()) {//validate the field
    var self = this
    //check the server for the entered nic
    axios.get(`http://localhost:8401/customer/searchbynic?nic=${this.state.nicsearchtext}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ customerdata: response.data });//assign customer data in the database to new states to be used
          self.setState({ name: self.state.customerdata[0].name });
          self.setState({ address: self.state.customerdata[0].address });
          self.setState({ phone: self.state.customerdata[0].phoneNo });
          self.setState({ nic: self.state.customerdata[0].nic });
        } else {
          Swal.fire("No Data!", "", "error");
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ loading: true });
        Swal.fire("Error!", "Server Failed!", "error");
      });
    }
  }
//called to search vehicle details by registration number
  searchByRegNo = event => {
    event.preventDefault();
    if (this.validateregnosearchtext()) {//validate the field
    var self = this
    //get vehicle data for the entered registration number
    axios.get(`http://localhost:8401/vehicle/searchbyregno?regNo=${this.state.regnosearchtext}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ vehicledata: response.data });
          console.log(self.state.vehicledata[0].make);//set new states for the vehicle details came from database
          self.setState({ regNo: self.state.vehicledata[0].regNo });
          self.setState({ make: self.state.vehicledata[0].make });
          self.setState({ model: self.state.vehicledata[0].model });

        } else {
          Swal.fire("No Data!", "", "error");
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ loading: true });
        Swal.fire("Error!", "Server Failed!", "error");
      });
    }
  }
//only need for a user who registered for both repair and service ceneter
//check wheather that the current task that is adding is for only a vehicle service,
//or only a vehicle repair or for both of them
  handleselect = e => {
    e.preventDefault();
    this.setState({servicetype: e.target.value});
  }
//called to start the task after filling required detasils
  handleSubmit = event => {
    event.preventDefault();
    if (this.validate()) {//validate and check for the input fields for errors
      //send customer data to the server or update customer data
      console.log(this.state.name+ " "+this.state.address+" "+this.state.phone+" "+this.state.nic);
    axios.post(`http://localhost:8401/customer/create?name=${this.state.name}&address=${this.state.address}&phoneNo=${this.state.phone}&nic=${this.state.nic}`)
      .then(res => {
        if (res.status === 201 || res.status === 200) {
          //send vehicle data to the server or update vehicle data
          axios.post(`http://localhost:8401/vehicle/create?regNo=${this.state.regNo}&make=${this.state.make}&model=${this.state.model}&milage=${this.state.milage}`)
            .then(res2 => {
              if (res2.status === 201 || res2.status === 200) {
                //create the task with the given details
                axios.post(`http://localhost:8401/task/create?regNo=${res2.data.body.regNo}&nic=${res.data.body.nic}&userEmail=${this.state.usermail}&additionalInfo=${this.state.additionalinfo}&serviceType=${this.state.servicetype}`)
                .then(res3 => {
                  if (res3.status === 201 || res3.status === 200) {
                    Swal.fire({
                      icon: 'success',
                      title: 'New Task Added!',
                      html: `<table className="viewTable" cellpadding="5" cellspacing="5">
                                      <tr>
                                        <td align="left">Customer Name :</td>
                                        <td align="left" className="leftPad">${res.data.body.name}</td>
                                      </tr>
                                      <tr>
                                        <td align="left">Vehicle :</td>
                                        <td align="left" className="leftPad">${res2.data.body.regNo}</td>
                                      </tr>
                                      <tr>
                                        <td align="left">Mobile :</td>
                                        <td align="left" className="leftPad">${res.data.body.phoneNo}</td>
                                      </tr>
                                  </table>`,
                    })
                    this.handleReset();//reset the fields
                  }
                }
    
                )
              }
            }

            )//second axios -post then end here
            .catch(err => {
              this.setState({ loading: true });
              Swal.fire({
                icon: 'error',
                title: 'Server error occured',
                html: '<p>Try again in few minutes</p>'
              })
            });
        }
      })
      .catch(err => {
        this.setState({ loading: true });
        Swal.fire({
          icon: 'error',
          title: 'Server error occured when adding customer',
          html: '<p>Try again in few minutes</p>'
        })
      });
    }
  }

  //validate the input fields
  validate() {

    let isValid = true;
    let errors = {};
    if (!this.state.name) {
      isValid = false;
      errors["name"] = "Please enter name";
    }
    if (!this.state.make) {
      isValid = false;
      errors["make"] = "Please enter make";
    }
    if (typeof this.state.regNo !== "undefined") {
      var pattern = new RegExp(/^([a-zA-Z]{1,3}|((?!0*-)[0-9]{0,3}))-[0-9]{4}(?<!0{4})/);
      if (!pattern.test(this.state.regNo)) {
        isValid = false;
        errors["regNo"] = "Please enter valid Reg No.";
      }
    }
    if (!this.state.model) {
      isValid = false;
      errors["model"] = "Please enter model";
    }
    if (!this.state.phone || this.state.phone.length !== 10 || isNaN(this.state.phone)) {
      isValid = false;
      errors["phone"] = "Please enter a valid number";
    }
    if (!this.state.milage) {
      isValid = false;
      errors["milage"] = "Please enter milage";
    }
    
    if (typeof this.state.nic !== "undefined") {
      var pattern = new RegExp(/^([0-9]{9}[x|X|v|V]|[0-9]{12})$/m);
      if (!pattern.test(this.state.nic)) {
        isValid = false;
        errors["nic"] = "Please enter valid NIC.";
      }
    }



    if (!this.state.servicetype) {
      isValid = false;
      errors["servicetypeerror"] = "Please select the service type";
    }
    this.setState({
      errors: errors
    });
    return isValid;
  }

  //this is only to validate the search text field for NIC
  validatenicsearchtext() {
    let nicsearchtext = this.state.nicsearchtext;
    let isValid = true;
    let errors = {};
    if (!nicsearchtext) {
      isValid = false;
      errors["nicsearchtext"] = "Please enter NIC";
    }
    this.setState({
      errors: errors
    });
    return isValid;
  }

  //only need for a user who registered for both repair and service ceneter
//check wheather that the current task that is adding is for only a vehicle service,
//or only a vehicle repair or for both of them
  selectionboxhandlerbyuserservicetype = () => {
    if(this.state.userservicetype === "Both"){
      return(
        <Form.Group as={Col}>
        <Form.Label>Select Service Type</Form.Label>
                    <Form.Control name="servicetypeselection"  as="select" value={this.state.servicetype} onChange={this.handleselect}>
                    <option key="1" value=""></option>
                      <option key="2" value="Vehicle Service">Vehicle Service</option>
                      <option key="3" value="Vehicle Repair">Vehicle Repair</option>
                      <option key="4" value="Both">Both</option>
        </Form.Control>
        <div className="text-danger">{this.state.errors.servicetypeerror}</div>
        </Form.Group>
      )
    }
  }
 //this is only to validate the search text field for Vehicle registration number
  validateregnosearchtext() {
    let regnosearchtext = this.state.regnosearchtext;
    let isValid = true;
    let errors = {};
    if (!regnosearchtext) {
      isValid = false;
      errors["regnosearchtext"] = "Please enter Reg No";
    }
    this.setState({
      errors: errors
    });
    return isValid;
  }

//render the Add new task page
  render() {
    return (
      <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "relative", width: "100%", height: "100%" }}>
        <div style={{textAlign:"center", marginTop:"1.5rem", marginBottom:'1rem', fontFamily:"sans-serif"}}><h1 style={{color:'white'}}>Add New Task</h1></div>
        <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>
          <Card border="primary" style={{width: "70%", height: "100%", margin:"auto", padding: '0.2rem'}}>
            
            <Card.Body>
              <Form onSubmit={this.handleSubmit}>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Label>Customer Details</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Label>Vehicle Details</Form.Label>
                  </Form.Group>
                </Form.Row>

                <Form.Row>
                  <Form.Group as={Col}>
                    <div class="form-group">
                    <Form inline >
                      <FormControl type="text" placeholder="Search By NIC" name="nicsearchtext" value={this.state.nicsearchtext} onChange={this.handleChange} />
                      <Button style={{ marginLeft: "1rem" }} variant="outline-success" onClick={this.searchByNic}>Search</Button>
                    </Form>
                    <div className="text-danger">{this.state.errors.nicsearchtext}</div>
                    </div>
                  </Form.Group>
                  <Form.Group as={Col}>
                  <div class="form-group">
                    <Form inline >
                      <FormControl type="text" placeholder="Search By Reg No." name="regnosearchtext" value={this.state.regnosearchtext} onChange={this.handleChange} />
                      <Button style={{ marginLeft: "1rem" }} variant="outline-success" onClick={this.searchByRegNo}>Search</Button>
                    </Form>
                    <div className="text-danger">{this.state.errors.regnosearchtext}</div>
                  </div>
                  </Form.Group>
                </Form.Row>

                <Form.Row>
                  <Form.Group as={Col}>
                  <div class="form-group">
                    <Form.Label>Name</Form.Label>
                    <Form.Control  placeholder="Enter name" name="name" value={this.state.name} onChange={this.handleChange} />
                    <div className="text-danger">{this.state.errors.name}</div>
                  </div>
                  </Form.Group>
                  <Form.Group as={Col}>
                    <div class="form-group">
                    <Form.Label>Vehicle Registration Number</Form.Label>
                    <Form.Control  placeholder="Enter vehicle registration number" name="regNo" value={this.state.regNo} onChange={this.handleChange} />
                    <div className="text-danger">{this.state.errors.regNo}</div>
                    </div>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Label>Address</Form.Label>
                    <Form.Control  placeholder="Enter address" name="address" value={this.state.address} onChange={this.handleChange} />
                  </Form.Group>
                  <Form.Group as={Col}>
                  <div class="form-group">
                    <Form.Label>Made by</Form.Label>
                    <Form.Control  placeholder="Made by" name="make" value={this.state.make} onChange={this.handleChange} />
                    <div className="text-danger">{this.state.errors.make}</div>
                  </div>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                  <div class="form-group">
                    <Form.Label>Phone No.</Form.Label>
                    <Form.Control  placeholder="Enter Phone number" name="phone" value={this.state.phone} onChange={this.handleChange} />
                    <div className="text-danger">{this.state.errors.phone}</div>
                  </div>
                  </Form.Group>
                  <Form.Group as={Col}>
                  <div class="form-group">
                    <Form.Label>Model</Form.Label>
                    <Form.Control  placeholder="Enter Model" name="model" value={this.state.model} onChange={this.handleChange} />
                    <div className="text-danger">{this.state.errors.model}</div>
                  </div>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                  <div class="form-group">
                    <Form.Label>Enter NIC</Form.Label>
                    <Form.Control  placeholder="Enter NIC" name="nic" value={this.state.nic} onChange={this.handleChange} />
                    <div className="text-danger">{this.state.errors.nic}</div>
                  </div>
                  </Form.Group>
                  <Form.Group as={Col}>
                  <div class="form-group">
                    <Form.Label>Milage (In Km)</Form.Label>
                    <Form.Control placeholder="Enter milage" name="milage" value={this.state.milage} onChange={this.handleChange} />
                    <div className="text-danger">{this.state.errors.milage}</div>
                  </div>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Label>Additional Info:</Form.Label>
                    <Form.Control as="textarea" rows={3} placeholder="Any Additional Information" name="additionalinfo" value={this.state.additionalinfo} onChange={this.handleChange}/>
                  </Form.Group>
                  
                    {this.selectionboxhandlerbyuserservicetype()}
                </Form.Row>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
                <Button variadatant="primary" type="button" style={{ marginLeft: "3rem" }} onClick={this.handleReset}>
                  Reset
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Spin>
      </div>
    );
  }
}

export default AddNewTask;