import React, { Component } from 'react'
import { Spin } from "antd";
import "antd/dist/antd.css";
import Background from '../../Images/background.jpg';
import { Form, Col, Card, Button } from 'react-bootstrap'
import Swal from "sweetalert2";
import axios from "axios";
import { TextField,InputAdornment, IconButton } from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

class UserProfile extends Component {
//assigning initial states for the values
    constructor(props) {
        super(props);
        this.state = {
            usermail: JSON.parse(sessionStorage.getItem("usermail")),
            loading: false,
            inputvalues: {
                title: ''
            },
            type: 'password',
            passwords: {
                currentpassword: '',
                newpassword:'',
                confirmpassword:'',
            },
            showCurrentPassword: false,
            showNewPassword: false,
            showConfirmPassword: false,
        }
        this.onChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
//control the data when rendering this page
    componentDidMount() {
        var self = this
        //get user's center title,servicetype from server
        axios.get(`http://localhost:8401/user/searchemail?email=${this.state.usermail}`)
            .then(function (response) {
                console.log(response.data.email.length);
                if (response.data.email.length > 0) {

                    self.setState({ usertitle: response.data.title });
                    self.setState({ userservicetype: response.data.servicetype });
                    //self.setState({ userpassword: response.data.password });
                }
            })
            .catch(function (error) {
                console.log(error);
                self.setState({ loading: true });
                Swal.fire("Error!", "Server Failed!", "error");
            });
    }
//called when values of title field is chanigng
    handleChange(e) {
        var oldState = this.state.inputvalues;
        var newState = { [e.target.name]: e.target.value };
        this.setState({ inputvalues: Object.assign(oldState, newState) });
    }
//called when values of password fields are changing
    onPasswordChanged(e){
        var oldState = this.state.passwords;
        var newState = { [e.target.name]: e.target.value };
        this.setState({ passwords: Object.assign(oldState, newState) });
    }
//called when click show/hide icon in current passsword
    handleClickShowCurrentPassword = () => {
        this.setState({
          showCurrentPassword: !this.state.showCurrentPassword
        });
      }
    //called when click and hold show/hide icon in current passsword
    handleMouseDownCurrentPassword = () => {
        this.setState({
            showCurrentPassword: !this.state.showCurrentPassword
        });
    }
//called when click show/hide icon in new passsword
    handleClickShowNewPassword = () => {
        this.setState({
          showNewPassword: !this.state.showNewPassword
        });
      }
    //called when click and hold show/hide icon in new passsword
    handleMouseDownNewPassword = () => {
        this.setState({
            showNewPassword: !this.state.showNewPassword
        });
    }
//called when click show/hide icon in confirm passsword
    handleClickShowConfirmPassword = () => {
        this.setState({
            showConfirmPassword: !this.state.showConfirmPassword
        });
      }
    //called when click and hold show/hide icon in confirm passsword
    handleMouseDownConfirmPassword = () => {
        this.setState({
            showConfirmPassword: !this.state.showConfirmPassword
        });
    }
//called when click the button change password
    changePassword(event){
        if(!this.state.passwords.currentpassword || !this.state.passwords.newpassword || !this.state.passwords.confirmpassword){
            Swal.fire(
                'Fill the fields!',
                '',
                'error'
            )
        }
        else if(this.state.passwords.newpassword.length<8){
            Swal.fire(
                'Password must be 8 characters!',
                '',
                'error'
            )
        }
        else if(this.state.passwords.newpassword !== this.state.passwords.confirmpassword){
            Swal.fire(
                'Passwords not matching!',
                '',
                'error'
            )
        }
        else if(this.state.passwords.currentpassword === this.state.passwords.newpassword){
            Swal.fire(
                'Check your passwords!',
                '',
                'error'
            )
        }
        else{
            axios.get(`http://localhost:8401/user/signin?email=${this.state.usermail}&password=${this.state.passwords.currentpassword}`)
                .then(res => {
                    //response id undefined, password for the email is not matching
                    if (res.data[0] === undefined) {
                        Swal.fire("Current Password is invalid!", "", "error");
                    } else {//password is correct, update the password
                        axios.post(`http://localhost:8401/user/updatepassword?email=${this.state.usermail}&newpassword=${this.state.passwords.newpassword}`)
                            .then(res => {
                                if (res.status === 201 || res.status === 200) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Password Changed!',
                                    })
                                    this.setState({ passwords: {
                                        currentpassword: '',
                                        newpassword:'',
                                        confirmpassword:'',
                                    } });
                                }
                            })
                            .catch(err => {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Server error occured',
                                    html: '<p>Try again in few minutes</p>'
                                })
                            });
                    }
                })
                .catch(err => {
                    console.log(err);
                    Swal.fire("Error!", "Server Failed", "error");
                });
        }
    }
//called to delete the user account
    deleteAccount(event){
        event.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.isConfirmed) {
                  axios.post(`http://localhost:8401/user/deleteuseraccount?email=${this.state.usermail}`)
                  .then(res => {
                      if (res.status === 201 || res.status === 200) {
                          Swal.fire({
                              icon: 'success',
                              title: 'Account Deleted!'
                          }).then((result) => {
                            if (result.isConfirmed) {
                              window.location.href = '/'
                            }
                          });
                      }
                  })
                  .catch(err => {
                      this.setState({ loading: true });
                      Swal.fire({
                          icon: 'error',
                          title: 'Server error occured',
                          html: '<p>Try again in few minutes</p>'
                      })
                  });
            }
        })
    }
//called to update the title when button clicked
    handleSubmit(event) {
        event.preventDefault();
        if (this.state.inputvalues.title.length > 0) {
            //send new title to the server
            axios.post(`http://localhost:8401/user/updatetitle?email=${this.state.usermail}&title=${this.state.inputvalues.title}`)
                .then(res => {
                    if (res.status === 201 || res.status === 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Title Changed!',
                        })
                        this.setState({ usertitle: this.state.inputvalues.title });//update the user title input value

                    } 
                })
                .catch(err => {
                    this.setState({ loading: true });
                    Swal.fire({
                        icon: 'error',
                        title: 'Server error occured',
                        html: '<p>Try again in few minutes</p>'
                    })
                });
        }
        else {
            Swal.fire({
                icon: 'info',
                title: 'Enter a new title!'
            })
        }
    }
//render the user profile component
    render() {
        return (
            <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
                <div style={{ textAlign: "center", marginTop: "1.5rem", marginBottom:'1rem', fontFamily: "sans-serif" }}><h1 style={{color:'white'}}>Manage Your Profile Data</h1></div>
                <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>
                    <Card border="primary" style={{ width: "60%", height: "100%", marginBottom: "1rem", margin: "auto", padding: '0.2rem' }}>
                        <Card.Body>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Registered Email</Form.Label>
                                    <Form.Control disabled name="email" value={this.state.usermail} />
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Service Type</Form.Label>
                                    <Form.Control disabled name="servicetype" value={this.state.userservicetype} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control placeholder={this.state.usertitle} onChange={this.onChange} name="title" value={this.state.inputvalues.title} />
                                    <Button onClick={this.handleSubmit} style={{ marginTop: '1rem' }} variant="primary" type="submit">Change Title</Button>
                                </Form.Group>
                            </Form.Row>
                            <div style={{ marginTop: '2rem' }}><b>Change Password</b></div>
                            <Form.Row style={{ marginTop: '1rem' }}>
                                <Form.Group as={Col}>
                                    <Form.Label>Current Password</Form.Label>
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        value={this.state.passwords.currentpassword}
                                        onChange={this.onPasswordChanged.bind(this)}
                                        fullWidth
                                        name="currentpassword"
                                        label="Current Password"
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="toggle password visibility"
                                                        onClick={this.handleClickShowCurrentPassword}
                                                        onMouseDown={this.handleMouseDownCurrentPassword}
                                                    >
                                                        {this.state.showCurrentPassword ? <Visibility /> : <VisibilityOff />}
                                                    </IconButton>
                                                </InputAdornment>
                                            )
                                        }}
                                        type={this.state.showCurrentPassword ? "text" : "password"}
                                        id="currentpassword"
                                        autoComplete="current-password"
                                    />
                                    <Button onClick={this.changePassword.bind(this)} style={{ marginTop: '1rem' }} variant="primary" type="button">Change Password</Button>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>New Password</Form.Label>
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        value={this.state.passwords.newpassword}
                                        onChange={this.onPasswordChanged.bind(this)}
                                        fullWidth
                                        name="newpassword"
                                        label="New Password"
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="toggle password visibility"
                                                        onClick={this.handleClickShowNewPassword}
                                                        onMouseDown={this.handleMouseDownNewPassword}
                                                    >
                                                        {this.state.showNewPassword ? <Visibility /> : <VisibilityOff />}
                                                    </IconButton>
                                                </InputAdornment>
                                            )
                                        }}
                                        type={this.state.showNewPassword ? "text" : "password"}
                                        id="newpassword"
                                        autoComplete="new-password"
                                    />
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Confirm Password</Form.Label>
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        value={this.state.passwords.confirmpassword}
                                        onChange={this.onPasswordChanged.bind(this)}
                                        fullWidth
                                        name="confirmpassword"
                                        label="Confirm Password"
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="toggle password visibility"
                                                        onClick={this.handleClickShowConfirmPassword}
                                                        onMouseDown={this.handleMouseDownConfirmPassword}
                                                    >
                                                        {this.state.showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                                                    </IconButton>
                                                </InputAdornment>
                                            )
                                        }}
                                        type={this.state.showConfirmPassword ? "text" : "password"}
                                        id="confirmpassword"
                                        autoComplete="confirm-password"
                                    />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} style={{textAlign:"center"}}>
                                    <Button onClick={this.deleteAccount.bind(this)} style={{ marginTop: '1rem' }} variant="danger" type="button">Delete Account</Button>
                                </Form.Group>
                            </Form.Row>
                        </Card.Body>
                    </Card>
                </Spin>
            </div>
        )
    }

}

export default UserProfile