import React, { Component } from 'react'
import axios from 'axios'
import Swal from 'sweetalert2'
import { Table, Button } from 'react-bootstrap'
//import Pagination from 'react-bootstrap/Pagination'
import { Spin } from 'antd';
import Background from '../../Images/background.jpg'

class ViewItems extends Component {
//assigning initial states for the values
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      userdata: JSON.parse(sessionStorage.getItem("usermail")),
      currentPage: 1,
      itemsPerPage: 8,
      loading: false,
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }
//control the data when rendering this page
  componentDidMount() {
    var self = this
    axios.get(`http://localhost:8401/item/getallitemsbyemail?email=${this.state.userdata}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ data: response.data });
          
        } else {
          self.setState({ loading: true });
          self.setState({ data: [] });
          Swal.fire("No Data!", "", "error");
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ loading: true });
        Swal.fire("Error!", "Server Failed!", "error");
      });
  }
//render added item list by the user to the inventory
  render() {

    
//rendering the item info to the table
    const details = this.state.data.map((item, index) => (
      <tr style={{display:'table', width:'100%',tableLayout:'fixed'}}>
        <td key={index}>{item.name}</td>
        <td key={index}>{item.category}</td>
        <td align="center"><Button variant="outline-light" onClick={() => {
          Swal.fire({
            showCancelButton: true,
            showDenyButton: true,
            title: 'Are you sure to delete?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showConfirmButton: false,
            denyButtonText: "Delete",
            width: `40%`,
          }).then((result) => {
            if (result.isDenied) {
              //delete item from the list
              axios.post(`http://localhost:8401/item/deletebyitemid?id=${item.id}`)
              .then(res => {
                  if (res.status === 201 || res.status === 200) {
                      Swal.fire(
                          'Deleted!',
                          'Item deleted',
                          'success'
                      )
                      this.componentDidMount();//re render the page
                  }
              })
              .catch(err => {
                  Swal.fire({
                      icon: 'error',
                      title: 'Server error occured',
                      html: '<p>Try again in few minutes</p>'
                  })
              });
            }
          })
        }}>Delete</Button></td>
      </tr>
    ))

    return (
      <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
                <div style={{textAlign:"center", marginTop:"1.5rem", fontFamily:"sans-serif"}}><h1 style={{color:'white'}}>My Items</h1></div>
                <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>
        <div style={{ margin: "2rem" }}>
          <Table style={{margin:'auto', width:'70%'}} bordered="true" variant="dark" >
            <thead style={{display:'table', width:'100%',tableLayout:'fixed'}}>
              <tr style={{ backgroundColor: "#9d46fa" }}>
                <th>Item Name</th>
                <th >Category</th>
                <th >Delete</th>
              </tr>
            </thead>
            <tbody style={{ display: 'block', overflowY: 'scroll', height: '500px' }}>
              {details}



            </tbody>

          </Table>
        </div>
        </Spin>
      </div>
    )
  }

}

export default ViewItems