import React, { Component } from 'react'
import { Form, Col, Button, Card, FormControl } from 'react-bootstrap'
import axios from 'axios'
import Swal from 'sweetalert2'
import { Spin, Select } from "antd";
import Background from '../../Images/background.jpg';

class AddItem extends Component {
  //assigning initial states for the values
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      loading: false,
      categorytexterror: '',
      categoryInput: '',
      userdata: JSON.parse(sessionStorage.getItem("usermail")),
      errors: {},
      categoryList: []
    }
  }
  //control the data when rendering this page
  componentDidMount() {
    var self = this
    //get user's category list from the database
    axios.get(`http://localhost:8401/category/getAllByUserEmail?email=${this.state.userdata}`)
      .then(function (response) {
        if (response.data.length > 0) {
          self.setState({ categoryList: response.data });
        } else {
          Swal.fire("No Categories Found!", "", "info");
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ loading: true });
        Swal.fire("Error!", "Server Failed!", "error");
      });
  }

  //resetting the fields
  handleReset = () => {
    this.setState({
      name: '',
      errors: {},
      itemname: '',
      categoryInput: ''
    });
  };
  //called whmarginLeft: "1rem"en inputs are chanigng
  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  }

  handleCateogryChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  }
  //called when need to validate fields
  validate() {

    let isValid = true;
    let errors = {};
    if (!this.state.name) {
      isValid = false;
      errors["name"] = "Please enter name";
    }
    if (!this.state.itemname) {
      isValid = false;
      errors["category"] = "Please select category";
    }
    this.setState({
      errors: errors
    });
    return isValid;
  }
  //called when adding new item
  handleSubmit = event => {
    event.preventDefault();
    if (this.validate()) {
      //send item's details to server
      axios.post(`http://localhost:8401/item/create?name=${this.state.name}&categoryName=${this.state.itemname}&email=${this.state.userdata}`)
        .then(res => {
          if (res.status === 201 || res.status === 200) {
            if(res.data === "Item Exist"){
              Swal.fire(
                'Item Exist!',
                '',
                'error'
              )
            }
            else{
              Swal.fire({
                icon: 'success',
                title: 'Success!',
                html: `<table className="viewTable" cellpadding="5" cellspacing="5">
                                <tr>
                                  <td align="left">Item Name :</td>
                                  <td align="left" className="leftPad">${res.data.body.name}</td>
                                </tr>
                                <tr>
                                  <td align="left">Category :</td>
                                  <td align="left" className="leftPad">${res.data.body.category}</td>
                                </tr>
                            </table>`,
              })
              this.handleReset();//call reset fields
            }
          } 
        })
        .catch(err => {
          Swal.fire({
            icon: 'error',
            title: 'Server error occured',
            html: '<p>Try again in few minutes</p>'
          })
        });
    }
  }
  //called and change state when selecting categories
  onSelectChange(value) {
    console.log(`selected ${value}`);
    this.setState({
      itemname: value
    });
  }

  addCategory = () => {

    if (!this.state.categoryInput) {
      this.setState({
        categorytexterror: "Please Enter a category name"
      });
    }
    else {
      this.setState({
        categorytexterror: ""
      });
      //send and add category name to server and database
      axios.post(`http://localhost:8401/category/create?email=${this.state.userdata}&categoryName=${this.state.categoryInput}`)
        .then(res => {
          if (res.status === 201 || res.status === 200) {
            if(res.data === "Category Exist"){
              Swal.fire({
                icon: 'error',
                title: 'Category Exists!',
              });
            }
            else{
              Swal.fire({
                icon: 'success',
                title: 'Category Added!',
              });
              this.setState({
                categoryInput: ""
              });
              this.componentDidMount();
            }
          }
        })
        .catch(err => {
          this.setState({ loading: true });
          Swal.fire({
            icon: 'error',
            title: 'Server error occured',
            html: '<p>Try again in few minutes</p>'
          })
        });
    }


  }


  render() {

    const { Option } = Select;
    //called when adding new category
    
    //add added categories to the drop down
    const loadedcategories = this.state.categoryList.map((categories) => (
      <Option value={categories.categoryName}>{categories.categoryName}</Option>
    ))
    // render add item page , item section,add category section
    return (
      <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
        <div style={{ textAlign: "center", marginTop: "1.5rem", marginBottom: '1rem', fontFamily: "sans-serif" }}><h2 style={{ color: 'white' }}>Add Items To Your Inventory</h2></div>
        <Spin size="large" tip="Loading..." spinning={this.state.loading} delay={30}>
          <Card border="primary" style={{ width: "60%", height: "100%", marginBottom: "1rem", margin: "auto", padding: '0.2rem' }}>
            <Card.Body>
              <Form onSubmit={this.handleSubmit}>
                <Form.Row>
                  <Form.Group as={Col}>
                    <div class="form-group">
                      <Form.Label>Item name</Form.Label>
                      <Form.Control placeholder="Enter item name" name="name" value={this.state.name} onChange={this.handleChange} />
                      <div className="text-danger">{this.state.errors.name}</div>
                    </div>
                  </Form.Group>
                </Form.Row>
                <Form.Row>

                  <Form.Group as={Col}>
                    <div class="form-group">
                      <Form.Label>Select Category</Form.Label>
                      <Select
                        showSearch
                        value={this.state.itemname}
                        allowClear
                        size="large"
                        style={{ width: "100%" }}
                        placeholder="Select a category"
                        optionFilterProp="children"
                        onChange={this.onSelectChange.bind(this)}
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {loadedcategories}
                      </Select>
                      <div className="text-danger">{this.state.errors.category}</div>
                    </div>
                  </Form.Group>
                </Form.Row>
                <Button variant="primary" type="submit">
                  Add
                </Button>
                <Button style={{ marginLeft: "2rem" }} onClick={this.handleReset} variant="primary" type="button">
                  Clear
                </Button>

              </Form>
            </Card.Body>
          </Card>
          {/*add category section */}
          <div style={{ textAlign: "center", marginTop: "4rem", marginBottom: '1rem', fontFamily: "sans-serif" }}><h2 style={{ color: 'white' }}>Add Categories</h2></div>
          <Card border="primary" style={{ width: "60%", height: "100%", marginBottom: "1rem", margin: "auto", padding: '0.2rem' }}>
            <Card.Body>
              <div>
              <Form.Label>Category Name</Form.Label>
                <Form.Row>
                  
                  <Form.Group>
                    <div class="form-group">
                    <Form inline >
                      <FormControl style={{ width:'41rem' }} type="text" placeholder="Enter a Category Name" name="categoryInput" value={this.state.categoryInput} onChange={this.handleCateogryChange} />
                      <Button style={{ marginLeft: "1rem" }} variant="primary" onClick={this.addCategory}>Add</Button>
                    </Form>
                    <div className="text-danger">{this.state.categorytexterror}</div>
                    </div>
                  </Form.Group>
                </Form.Row>
              </div>

            </Card.Body>
          </Card>
        </Spin>
      </div>
    )
  }

}

export default AddItem