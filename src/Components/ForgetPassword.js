import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import { TextField } from "@material-ui/core";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import logo from '../Images/auto work logo 2.png';
import Swal from "sweetalert2";
import axios from "axios";
import { Spin } from "antd"
import NavBar from './MainNav';
import Background from '../Images/4.jpg'

export default class ForgetPassword extends React.Component {

  //assigning primary states for the values
  constructor(props) {
    super(props);
    this.state = {
      inputvalues: {
        email: ''
      },
      loading: false,
    };
    this.onChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  //called when changing email imput
  handleChange(e) {
    var oldState = this.state.inputvalues;
    var newState = { [e.target.name]: e.target.value };

    this.setState({ inputvalues: Object.assign(oldState, newState) });
  }
//called when rendering this component to clear any cookies or data
  componentDidMount(){
    sessionStorage.clear();
  }

  //called when request button pressed
  handleSubmit(event) {
    event.preventDefault();

    //checking that the email is null or not
    if (this.state.inputvalues.email === '') {
      Swal.fire("Please Enter Your Email!", "", "error");
    }
    //checking that the entered email is a valid email
    else if (!new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i).test(this.state.inputvalues.email)) {
      Swal.fire("Please Enter a valid Email!", "", "error");
    }
    //if valid do the actions for resetting password
    else {
      var self = this
      //search the entered email in the database
      axios.get(`http://localhost:8401/user/searchemail?email=${this.state.inputvalues.email}`)
        .then(function (response) {
          if (response.data.email === undefined) {
            Swal.fire("Email Not Present!", "", "error");
          } else {
            Swal.fire({
              icon: 'info',
              title: 'Loading!',
              text:'Please Wait',
              showConfirmButton:false
            })
            //send data to the server to do the password resetting
            axios.post(`http://localhost:8401/email/passwordreset?email=${response.data.email}&password=${response.data.password}`)
              .then(res => {
                console.log(res.data);
                if (res.status === 201 || res.status === 200) {

                  Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    text:'Check your email to continue with reseting password'
                  }).then((result) => {
                    if (result.isConfirmed) {
                      window.location.href = '/'
                    }
                  })
                  
                }
              })
              .catch(function (error) {
                self.setState({loading:true});
                console.log(error);
                Swal.fire("Error!", "Server Failed!", "error");
              });
          }
        })
        .catch(function (error) {
          self.setState({loading:true});
          console.log(error);
          Swal.fire("Error!", "Server Failed!", "error");
        });
    }
  }

  render() {
    return (
      <div>
        {/*Render the main navigation bar and forget passsword component */}
        <div><NavBar /></div>
        <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>
        <Spin style={{marginTop:'20rem'}} size="large" tip="Loading..." spinning={this.state.loading} delay={30}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div style={{ marginTop: "8rem", marginBottom: "4rem" }} >
              <img src={logo} width="100%" height="100%" alt="logo" />
              <Avatar style={{ backgroundColor: "#dc004e", marginLeft: "11rem" }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5" style={{ marginLeft: '7rem' }}>
                Reset Password
        </Typography>
              <form encType="multipart/form-data" onSubmit={this.handleSubmit}>
                <div class="form-group">
                  <TextField
                    variant="outlined"
                    margin="normal"
                    type="text"
                    fullWidth
                    value={this.state.inputvalues.email}
                    onChange={this.onChange}
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                  />
                </div>
                <div>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    style={{ marginTop: "1rem" }}
                  >
                    Request
          </Button>
                </div>
              </form>
            </div>
          </Container>
          </Spin>
        </div>
      </div>
    );
  }
}