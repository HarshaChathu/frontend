import React from 'react';
import { Navbar, Nav, Form, Button } from 'react-bootstrap';

export default class MainNav extends React.Component {
    render() {
        return (
            <div>
                {/*Structure of the Main navigation bar before logged in*/}
                <div>
                    <Navbar bg="dark" variant="dark">
                        <Navbar.Brand style={{fontWeight:"bold"}} href="/">AUTOWORKS</Navbar.Brand>
                        <Nav className="mr-auto">
                            <Nav.Link style={{fontWeight:"bold", color: "white"}} href="/">Home</Nav.Link>
                            <Nav.Link style={{fontWeight:"bold", color: "white"}} href="/aboutus">About us</Nav.Link>
                        </Nav>
                        <Form inline>
                            <Button href="/signin" style={{color:"white",fontWeight:"bold"}} variant="primary">Sign In</Button><p> &nbsp;&nbsp;</p>
                            <Button href="/signup" style={{color:"white",fontWeight:"bold"}} variant="primary">Register</Button>
                        </Form>
                    </Navbar>
                </div>
            </div>
        );
    }
}