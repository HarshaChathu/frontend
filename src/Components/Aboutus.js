import React from 'react';
import NavBar from './MainNav';
import Background from '../Images/2.jpg';
import Customers from '../Images/customers.png';
import Vehicle from '../Images/vehicle.jpg';
import Task from '../Images/task.jpg';
import Resource from '../Images/resource.jpg';
import { Card, CardGroup, Form, Button } from 'react-bootstrap'
import { SocialIcon } from 'react-social-icons';
import Swal from 'sweetalert2'
import axios from 'axios'
import { Spin } from "antd"

class Aboutus extends React.Component {

    //assigning primary states for the values
    constructor(props) {
        super(props);
        this.state = {
          email: '',
          feedback: '',
          loading:false
        }
      }

      //resetting the fields
      handleReset = () => {
        this.setState({
          email: '',
          feedback: ''
        });
      };

      //called when clicking submit button 
    handleSubmit = event => {
        event.preventDefault();
        if(this.state.email === '' || this.state.feedback=== ''){
            Swal.fire({
                icon: 'error',
                title: 'Please fill the fields!'
              })
        }
        else if (!new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i).test(this.state.email)) {
            Swal.fire({
                icon: 'error',
                title: 'Please enter a valid email!'
              })
        }
        else{
            //send feed backs directly to the server
            axios.post(`http://localhost:8401/feedback/create?email=${this.state.email}&feedback=${this.state.feedback}`)
            .then(res => {
              if (res.status === 201 || res.status === 200) {
                Swal.fire({
                  icon: 'success',
                  title: 'Feedback Sent',
                  text: 'Thank You!'
                })
                //called reset method
                this.handleReset();
              }
            })
            .catch(err => {
                this.setState({loading:true});
              Swal.fire({
                icon: 'error',
                title: 'Server error occured',
                html: '<p>Try again in few minutes</p>'
              })
            });
        }
    }

    //called when changing email and the feedback inputs
    onChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
      }

    render() {
        return (
            <div>
                {/*Render the main navigation bar and about us page */}
                <NavBar />
                <Spin style={{marginTop:'22rem'}} size="large" tip="Loading..." spinning={this.state.loading} delay={30}>
                <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "relative", width: "100%", height: "100%" }}>
                    <div style={{ marginTop: '2rem' }}>
                        <CardGroup style={{ width: '80rem', height: '5rem', margin: 'auto' }}>
                            <Card bg='primary' style={{ padding: '1rem' }}>
                                <Card.Img variant="top" src={Customers} />
                                <Card.Body>
                                    <Card.Title style={{color:'white' , fontWeight:'bold'}}>Manage Customers</Card.Title>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Save Customer Data</Card.Subtitle>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>View Your Customers</Card.Subtitle>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Edit Customer Data</Card.Subtitle>
                                </Card.Body>
                            </Card>
                            <Card bg='info' style={{ marginLeft: '1rem', padding: '1rem' }}>
                                <Card.Img variant="top" src={Vehicle} />
                                <Card.Body>
                                    <Card.Title style={{color:'white' , fontWeight:'bold'}}>Manage Vehicles</Card.Title>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Save Vehicle Data</Card.Subtitle>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>View Your Vehicles</Card.Subtitle>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Edit Vehicle Data</Card.Subtitle>
                                </Card.Body>
                            </Card>
                            <Card bg='primary' style={{ marginLeft: '1rem', padding: '1rem' }}>
                                <Card.Img variant="top" src={Resource} />
                                <Card.Body>
                                    <Card.Title style={{color:'white' , fontWeight:'bold'}}>Manage Resources</Card.Title>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Add Desired Items</Card.Subtitle>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Categorize Items</Card.Subtitle>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Add Items to Tasks</Card.Subtitle>
                                </Card.Body>
                            </Card>
                            <Card bg='info' style={{ marginLeft: '1rem', padding: '1rem' }}>
                                <Card.Img variant="top" src={Task} />
                                <Card.Body>
                                    <Card.Title style={{color:'white' , fontWeight:'bold'}}>Manage Tasks/Jobs</Card.Title>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Add Multiple Tasks</Card.Subtitle>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>View Your Tasks</Card.Subtitle>
                                    <Card.Subtitle style={{ marginTop: '1rem',color:'white' }}>Manage Your Tasks</Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </CardGroup>
                    </div>
                    <div style={{ marginTop: '28rem' }}>
                        <CardGroup style={{ width: '80rem', margin: 'auto' }}>
                            <Card bg='dark' text={'dark' === 'light' ? 'dark' : 'white'} style={{ margin: 'auto', padding: '1rem' }}>
                                <Card.Body>
                                    <Card.Title>Provide any feedback !</Card.Title>
                                    <Form>
                                        <Form.Group>
                                            <Form.Label>Email address</Form.Label>
                                            <Form.Control name="email" value={this.state.email} onChange={this.onChange} type="email" placeholder="Enter Your Email" />
                                        </Form.Group>
                                        <Form.Group>
                                            <Form.Control name="feedback" value={this.state.feedback} onChange={this.onChange} as="textarea" rows={2} placeholder="Enter Feedback"/>
                                        </Form.Group>
                                        <Form.Group>
                                            <Button type="submit" onClick={this.handleSubmit} varient="primary">Submit</Button>
                                        </Form.Group>
                                    </Form>
                                </Card.Body>
                            </Card>
                            <Card bg='dark' className="text-center" text={'dark' === 'light' ? 'dark' : 'white'} style={{  marginLeft: '1rem', padding: '1rem' }}>
                                <Card.Body>
                                    <Card.Title>Hotline :-  +94719040432/ +94779646295</Card.Title>
                                    <Card.Title>Email :- Harshachathuranga68@gmail.com</Card.Title>
                                    <SocialIcon style={{ marginTop:'1.5rem'}} url="https://www.facebook.com/Hcrangagp" />
                                    <SocialIcon style={{marginLeft:'1rem', marginTop:'1.5rem'}} url="https://www.instagram.com/hcranga/" />
                                    <SocialIcon style={{marginLeft:'1rem', marginTop:'1.5rem'}} url="https://twitter.com/Hcranga" />
                                </Card.Body>
                            </Card>
                        </CardGroup>
                    </div>
                </div>
                </Spin>
            </div>
        );
    }
}

export default Aboutus;