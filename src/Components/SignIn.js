import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button2 from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import { TextField, InputAdornment, IconButton } from "@material-ui/core";
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import logo from '../Images/auto work logo 2.png';
import NavBar from './MainNav';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Swal from "sweetalert2";
import axios from "axios";
import Background from '../Images/4.jpg'
import { notification } from 'antd';
import { SmileOutlined } from '@ant-design/icons';

export default class SignIn extends React.Component {

  //assigning primary states for the values
  constructor(props) {
    super(props);
    this.state = {
      inputvalues: {
        email: '',
        password: ''
      },
      showPassword: false,
      errors: {},
    };
    this.onChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

   //called when rendering this component and clear any saved data
  componentDidMount(){
    sessionStorage.clear();
  }

  //for password
  //when clicking the password show/hide icon
  handleClickShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }

  //for password
   //when click and hold the password show/hide icon
  handleMouseDownPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }
 //called when changing the inputs from email, password and set the values
  handleChange(e) {
    var oldState = this.state.inputvalues;
    var newState = { [e.target.name]: e.target.value };

    this.setState({ inputvalues: Object.assign(oldState, newState) });
  }

  //validing the inputs
  validate() {
    let input = this.state.inputvalues;
    let isValid = true;
    let errors = {};
    if (!input["email"]) {
      isValid = false;
      errors["email"] = "Please enter your email Address.";
    }
    if (typeof input["email"] !== "undefined") {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(input["email"])) {
        isValid = false;
        errors["email"] = "Please enter valid email address.";
      }
    }
    if (input["password"].length<8) {
      isValid = false;
      errors["password"] = "Password must be at least 8 characters.";
    }
    this.setState({
      errors: errors
    });
    return isValid;
  }

   //called when signing up button pressed
  handleSubmit(event) {
    event.preventDefault();
    if (this.validate()) {
      //send user inputs to the server
      axios
        .get(`http://localhost:8401/user/signin?email=${this.state.inputvalues.email}&password=${this.state.inputvalues.password}`)
        .then(res => {
          //response id undefined
          if (res.data[0] === undefined) {
            Swal.fire("Check Credentials!", "", "error");
          } else {//user exist but not activated
            if (res.data[0].activestatus === 0) {
              Swal.fire("Account not activated!", "", "error");
            }//active user can log in to the system
            else {
              //saving the user email,user service type to be used in users other components
              sessionStorage.setItem('usermail', JSON.stringify(res.data[0].email));
              sessionStorage.setItem('userservicetype', JSON.stringify(res.data[0].servicetype));
              this.props.history.push("/user/dashboard");
              notification.open({
                message: 'Hello !!!',
                description:
                  'Welcome to '+res.data[0].title,
                icon: <SmileOutlined style={{ color: '#108ee9' }} />,
              });
            }
            
          }
        })
        .catch(err => {
          console.log(err);
          Swal.fire("Error!", "Server Failed", "error");
        });
    }
  }

  render() {
    return (
      <div>
        {/*Render Main Navigation bar and sing in component */}
        <div><NavBar /></div>
        <div style={{backgroundImage: "url(" + Background + ")" ,backgroundAttachment: 'fixed',backgroundRepeat: 'no-repeat',backgroundSize: 'cover',backgroundPosition: 'center', position: "absolute", width: "100%", height: "100%" }}>
        <div style={{ marginTop: "4rem", marginBottom: "3rem" }}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div>
              <img src={logo} width="100%" height="100%" alt="autoworkslogo"/>
              <Avatar style={{ backgroundColor: "#dc004e", marginLeft: "11rem" }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5" style={{ marginLeft: "10rem" }}>
                Sign in
        </Typography>
              <form onSubmit={this.handleSubmit}>
                <div class="form-group">
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    value={this.state.inputvalues.email}
                    onChange={this.onChange}
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                  />
                  <div className="text-danger">{this.state.errors.email}</div>
                </div>
                <div class="form-group">
                  <TextField
                    variant="outlined"
                    margin="normal"
                    value={this.state.inputvalues.password}
                    onChange={this.onChange}
                    fullWidth
                    name="password"
                    label="Password"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                    type={this.state.showPassword ? "text" : "password"}
                    id="password"
                    autoComplete="current-password"
                  />
                  <div className="text-danger">{this.state.errors.password}</div>
                </div>
                <Button2
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                >
                  Sign In
          </Button2>
                <Grid container style={{marginTop:"1rem"}}>
                  <Grid item xs>
                    <Link href="/forgetpassword" variant="body2">
                      Forgot password?
              </Link>
                  </Grid>
                  <Grid item>
                    <Link href="/signup" variant="body2">
                      {"Don't have an account? Register Now"}
                    </Link>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Container>
        </div>
        </div>
      </div>
    );
  }
}