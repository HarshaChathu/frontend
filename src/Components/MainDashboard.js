import React from 'react';
import NavBar from './MainNav';
import Background from '../Images/1.jpg'

export default class MainDashboard extends React.Component {

    componentDidMount(){
        sessionStorage.clear();
      }
    
    render() {
        return (
            <div>
                {/*Render Navigation bar and the home page content */}
                <NavBar />
                <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "absolute", width: "100%", height: "100%" }}>

                    <div style={{textAlign:"center", margin:"auto", marginTop:"14rem"}}>
                        <h1 style={{color:"white", fontSize:70}}>Welcome to AutoWorks</h1>
                        <h2 style={{color:"white", marginTop:'6rem'}}>Sign up and register your business</h2>
                        <h2 style={{color:"white"}}>to access our facilities</h2>
                      
                    </div>
                </div>
            </div>
        );
    }
}