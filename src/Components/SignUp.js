import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import { TextField, InputAdornment, IconButton } from "@material-ui/core";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import logo from '../Images/auto work logo 2.png';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { Form, Input } from "antd";
import Swal from "sweetalert2";
import axios from "axios";
import NavBar from './MainNav';
import Background from '../Images/4.jpg'

export default class SignUp extends React.Component {

  constructor(props) {
    super(props);
    //assigning primary states for the values
    this.state = {
      inputvalues: {
        email: '',
        password: '',
        cpassword: '',
        title: ''
      },
      showPassword: false,
      showPassword2: false,
      errors: {},
    };
    this.onChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  //called when rendering this component and clear any saved data
  componentDidMount(){
    sessionStorage.clear();
  }
  //for password
  //when clicking the password show/hide icon
  handleClickShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }
  //for password
  //when click and hold the password show/hide icon
  handleMouseDownPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }

  //for confirm password
  //when clicking the password show/hide icon
  handleClickShowPassword2 = () => {
    this.setState({
      showPassword2: !this.state.showPassword2
    });
  }
  //for confirm password
    //when click and hold the password show/hide icon
  handleMouseDownPassword2 = () => {
    this.setState({
      showPassword2: !this.state.showPassword2
    });
  }

  //called when changing the file and assign new state for the selected file
  onFileChangeHandler = e => {
    e.preventDefault();
    this.setState({
      selectedFile: e.target.files[0]
    });
    console.log(e.target.files[0].type);
  };

  //called when changing the service type and assign new state for the selected service type
  onSelectChangeHandler = e => {
    e.preventDefault();
    this.setState({
      servicetype: e.target.value
    });
  }

  //called when changing the inputs from email, password, title and set the values
  handleChange(e) {
    var oldState = this.state.inputvalues;
    var newState = { [e.target.name]: e.target.value };
    this.setState({ inputvalues: Object.assign(oldState, newState) });
  }
//validating the inputs
  validate() {
    let input = this.state.inputvalues;
    let isValid = true;
    let errors = {};
    
    //check wheather the email is valid or not
    if (typeof input["email"] !== "undefined") {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(input["email"])) {
        isValid = false;
        errors["email"] = "Please enter valid email address.";
      }
    }
    //if selected servicetype is undefined
    if (!this.state.servicetype) {
      isValid = false;
      errors["servicetype"] = "Please select a service type";
    }
    //if the selected file is undefined
    if (!this.state.selectedFile) {
      isValid = false;
      errors["files"] = "Please select a file";
    }
    
    //if password is lessa than 8 characters
    if (input["password"].length<8) {
      isValid = false;
      errors["password"] = "Password must be at least 8 characters.";
    }
    //if confirm password is undefined
    if (!input["cpassword"]) {
      isValid = false;
      errors["cpassword"] = "Please enter your confirm password.";
    }
    //if title is undefined
    if (!input["title"]) {
      isValid = false;
      errors["title"] = "Please enter a title";
    }
    //if passwords are not matching
    if (typeof input["password"] !== "undefined" && typeof input["cpassword"] !== "undefined") {
      if (input["password"] !== input["cpassword"]) {
        isValid = false;
        errors["cpassword"] = "Passwords don't match.";
      }
    }
    //change the states of the error messages to render
    this.setState({
      errors: errors
    });
    return isValid;
  }

  //called when signing up button pressed
  handleSubmit(event) {
    event.preventDefault();
    if (this.validate()) {
      //appending the data to FormData because of the image file before sending to the server
      const formData = new FormData();
      formData.append("email", this.state.inputvalues.email);
      formData.append("password", this.state.inputvalues.password);
      formData.append("title", this.state.inputvalues.title);
      formData.append("servicetype", this.state.servicetype);
      formData.append("file", this.state.selectedFile);
      axios
        .post(`http://localhost:8401/user/create`, formData)
        .then(res => {
          if (res.data === 'User Exist') {
            //if response from the server is User Exist this message will visible
            Swal.fire("Already Registered!", "", "error");
          }
          else {
            //Swal is waiting until receiving response when sending email to the user
            Swal.fire({
              icon: 'info',
              title: 'Loading!',
              text:'Please Wait',
              showConfirmButton:false
            })
            axios.post(`http://localhost:8401/email/signupmessage?email=${this.state.inputvalues.email}`)
              .then(res => {
                console.log(res.data);
                if (res.status === 201 || res.status === 200) {
                  
                  Swal.fire({
                    icon: "success",
                    title: "Your Request has been sent!",
                    html: `<table className="viewTable" cellpadding="5" cellspacing="5">
                    <tr>
                    <td align="left">A confirmation email will be sent in next 1-2 days to the email ${this.state.inputvalues.email
                      }</td>
                    </tr>
                    </table>`
                  }).then((result) => {
                    //move to the home page when clicking ok button on the alert message
                    if (result.isConfirmed) {
                      window.location.href = '/'
                    }
                  });
                }
              })
              .catch(err => {
                console.log(err);
                Swal.fire("Error!", "Server failed", "error");
              });

            
          }
        })
        .catch(err => {
          console.log(err);
          Swal.fire("Error!", "Server Failed!", "error");
        });
    }
  }

  render() {
    return (
      <div>
        {/*Render Navigation bar and Sign up page components */}
        <div><NavBar /></div>
        <div style={{ backgroundImage: "url(" + Background + ")", overflow: 'scroll', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition: 'center', backgroundAttachment: 'fixed', position: "relative", width: "100%", height: "100%" }}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div style={{ marginTop: "4rem", marginBottom: "4rem" }} >
              <img src={logo} width="100%" height="100%" alt="logo" />
              <Avatar style={{ backgroundColor: "#dc004e", marginLeft: "11rem" }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5" style={{ marginLeft: "9.5rem" }}>
                Sign up
        </Typography>
              <form encType="multipart/form-data" onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <TextField
                    variant="outlined"
                    margin="normal"
                    type="text"
                    fullWidth
                    value={this.state.inputvalues.email}
                    onChange={this.onChange}
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                  />
                  <div className="text-danger">{this.state.errors.email}</div>
                </div>
                <div className="form-group">
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    value={this.state.inputvalues.password}
                    onChange={this.onChange}
                    name="password"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                    label="Password"
                    type={this.state.showPassword ? "text" : "password"}
                    id="password"
                    autoComplete="current-password"
                  />
                  <div className="text-danger">{this.state.errors.password}</div>
                </div>
                <div className="form-group">
                  <TextField
                    variant="outlined"
                    margin="normal"
                    value={this.state.inputvalues.cpassword}
                    onChange={this.onChange}
                    fullWidth
                    name="cpassword"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={this.handleClickShowPassword2}
                            onMouseDown={this.handleMouseDownPassword2}
                          >
                            {this.state.showPassword2 ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                    label="Confirm Password"
                    type={this.state.showPassword2 ? "text" : "password"}
                    id="cpassword"
                    autoComplete="current-password"
                  />
                  <div className="text-danger">{this.state.errors.cpassword}</div>
                </div>
                <div className="form-group">
                  <TextField
                    id="servicetype"
                    name="servicetype"
                    fullWidth
                    margin="normal"
                    onChange={this.onSelectChangeHandler}
                    select
                    label="Select Service Type"
                    variant="outlined"
                  >
                    <MenuItem key="1" value="Both">Both</MenuItem>
                    <MenuItem key="2" value="Service Center">Service Center</MenuItem>
                    <MenuItem key="3" value="Repair Center">Repair Center</MenuItem>
                  </TextField>
                  <div className="text-danger">{this.state.errors.servicetype}</div>
                </div>
                <div className="form-group">
                  <TextField
                    variant="outlined"
                    margin="normal"
                    type="text"
                    fullWidth
                    value={this.state.inputvalues.title}
                    onChange={this.onChange}
                    id="title"
                    label="Title"
                    name="title"
                    autoComplete="title"
                    autoFocus
                  />
                  <div className="text-danger">{this.state.errors.title}</div>
                </div>
                <div className="form-group">
                  <div style={{ marginTop: "2rem" }}><h5>Please Upload a image that shows the proof of residence</h5></div>
                  <Form.Item>
                    <Input
                      type="file"
                      name="file"
                      id="file"
                      size="large"
                      onChange={this.onFileChangeHandler}
                      accept=".png, .jpg, .jpeg"
                    />
                  </Form.Item>
                  <div className="text-danger">{this.state.errors.files}</div>
                </div>
                <div>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    style={{ marginTop: "1rem" }}
                  >
                    Register
          </Button>
                </div>
              </form>
            </div>
          </Container>
        </div>
      </div>
    );
  }
}