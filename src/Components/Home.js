import React, {Component} from 'react'
import {BrowserRouter, Route, Switch} from "react-router-dom";

import Dashboard from './NavLinks/Dashboard'
import AddNewTask from './NavLinks/AddNewTask'
import ViewCustomer from './NavLinks/ViewCustomer'
import ViewVehicle from './NavLinks/ViewVehicle'
import ViewTasks from './NavLinks/ViewTasks'
import AddItem from './NavLinks/AddItem'
import ViewItems from './NavLinks/ViewItems'
import ViewCategories from './NavLinks/ViewCategories'
import UserProfile from './NavLinks/UserProfile'
import AddItemsToTask from './NavLinks/AddItemsToTask'
import ViewInvoices from './NavLinks/ViewInvoices'
import Nav from './NavigationBar'

class Home extends Component{

    //managing the routing through links after user logged in
    render(){
        return(
            <div>
                {/*render the user dahsboard after logged in, and it's components*/}
                <Nav />
                <BrowserRouter>
                    <React.Fragment>
                        <Switch>
                            <Route exact path="/user/dashboard" component={Dashboard} />
                            <Route exact path="/user/task/addtask" component={AddNewTask} />
                            <Route exact path="/user/mydata/customers" component={ViewCustomer} />
                            <Route exact path="/user/mydata/vehicles" component={ViewVehicle} />
                            <Route exact path="/user/task/listtasks" component={ViewTasks} />
                            <Route exact path="/user/inventory/additem" component={AddItem} />
                            <Route exact path="/user/inventory/items" component={ViewItems} />
                            <Route exact path="/user/inventory/categories" component={ViewCategories} />
                            <Route exact path="/user/profile" component={UserProfile} />
                            <Route exact path="/user/task/additems" component={AddItemsToTask} />
                            <Route exact path="/user/task/viewinvoices" component={ViewInvoices} />
                        </Switch>
                    </React.Fragment>
                </BrowserRouter>
            </div>
        );
    }
}

export default Home;