import React from 'react';
import './App.css';
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom"

import NotFound from './Components/NotFound';
import Home from './Components/Home';
import MainDashBoard from './Components/MainDashboard';
import SignUp from './Components/SignUp';
import SignIn from './Components/SignIn';
import Aboutus from './Components/Aboutus';
import ForgetPassword from './Components/ForgetPassword';

//defining main routes in the website
function App() {
  return (
    <div>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={MainDashBoard}/>
        <Route exact path="/signin" component={SignIn}/>
        <Route exact path="/signup" component={SignUp}/> 
        <Route exact path="/aboutus" component={Aboutus}/> 
        <Route exact path="/forgetpassword" component={ForgetPassword}/> 
        <Route exact path={
          "/user/dashboard" | 
          "/user/task/addtask" |
          "/user/mydata/customers" |
          "/user/mydata/vehicles" |
          "/user/task/listtasks" |
          "/user/inventory/additem" |
          "/user/inventory/items" |
          "/user/inventory/categories" |
          "/user/profile" |
          "/user/task/additems" |
          "/user/task/viewinvoices"
        } component={Home} /> 
        <Route exact path="/404" component={NotFound} />
        <Redirect to="/404" />
      </Switch>
    </BrowserRouter>
    </div>
  );
}

export default App;
